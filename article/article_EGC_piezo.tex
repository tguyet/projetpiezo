%% -*- TeX -*- -*- FR -*-

%Avant propos : ces exemples de fichiers ont �t� mis � jour gr�ce �
%l'aide pr�cieuse de Gilbert Ritschard. Pour toute question ou
%remarque n'h�sitez pas � nous contacter : venturin@univ-tours.fr ou
%gilbert.ritschard@themes.unige.ch
%Version 3 2008-05-21
%Version 3.1 2012-11-26 Bruno Pinaud <bruno.pinaud@labri.fr>

\documentclass[a4paper,french]{rnti}
%\documentclass[a4paper,french,submission]{rnti}  %% pour soumission � EGC

%\documentclass[a4paper,french,noresume]{rnti} %% Pour papier de 2 pages

%%% Avec l'option "showlayout" vous obtenez les deux pages
%%% de contr�le des param�tres de mise en page.

%\documentclass[a4paper,footer,french,showlayout]{rnti}

%packages n�cessaires pour �crire des articles en fran�ais en utilisant les accents non latex.
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

%pour bien pr�senter les URL et autres adresses emails
\usepackage{url}

\usepackage{graphicx}
\input{macros}

% Titre court pour ent�te
\titrecourt{Pr�diction du niveau de nappes phr�atiques} 

% Noms auteurs pour ent�te :
%    Si un seul auteur, mettre : Initiale. NomPremierAuteur
%    Si deux auteurs, mettre : Initiale1. NomPremierAuteur et Initiale1. NomDeuxiemeAuteur
%    Si plus de deux auteurs, mettre comme ci-dessous
%
\nomcourt{L. Beuch{\'e}e et \textit{al.}}


\titre{Pr�diction du niveau de nappes phr�atiques~: comparaison d'approches locale, globale et hybride}

\auteur{Lola Beuch{\'e}e\affil{1},
        Thomas Guyet\affil{2},
        Simon Malinowski\affil{1}}

\affiliation{
    \affil{1}Univ. Rennes, CNRS, Inria/IRISA\\
    %
    \affil{2}Inria -- Centre Grenoble Rh�ne-Alpes\\
          thomas.guyet@inria.fr
 }

\resume{%
Cet article pr�sente l'exploration d'une m�thode autor�gressive de pr�vision d'une s�rie temporelle pour r�pondre au d�fi de la pr�diction du niveau de nappes phr�atiques. 
Une m�thode autor�gressive estime une valeur future d'une s�rie temporelle par r�gression � partir des valeurs historiques de la s�rie. Plusieurs m�thodes de r�gression peuvent alors �tre employ�es.
Dans cet article, on pr�sente des exp�rimentations visant � identifier la meilleure configuration pour pr�dire de mani�re pr�cise le niveau de nappes phr�atiques. On compare pour cela diff�rents pr�dicteurs, l'apprentissage de mod�le par s�rie ou par groupe de s�ries, et l'utilisation de donn�es exog�nes.
Des exp�rimentations intensives ont �t� men�es et nous permettent de conclure sur le choix de la m�thode que nous utiliserons pour r�pondre au d�fi.
%
}

\summary{%
This paper investigates an auto-regressive method for time series forecasting and applied to the challenge of predicting groundwater levels. 
An auto-regressive method estimates a next value of a time series by regression with the historical values of the series. Several regression methods can be used.
In this paper, experiments are presented to identify the best setting to accurately predict the groundwater levels. Different classifiers, different modes of learning (by series or by group of series), and different usages of exogenous data are compared.
Intensive experiments have been conducted and allow us to conclude on the best method we will use to answer the challenge.
}


\begin{document}

\section{Introduction}

Le d�fi EGC 2021 proposait d'explorer le niveau des nappes phr�atiques (niveaux pi�zom�triques) au travers des donn�es historiques qui sont mises � disposition par le BRGM\footnote{BRGM~: Bureau des Recherches G�ologiques et Mini�res}. Nous nous sommes plus particuli�rement int�ress�s � l'objectif de la pr�diction de l'�volution de ces niveaux � moyen terme (i.e. plusieurs mois).

L'enjeu de la pr�diction du niveau des nappes phr�atiques est l'aide � la gestion responsable d'une ressource essentielle pour diff�rents usages~: alimentation humaine (2/3 des volumes d'eau destin�e � l'alimentation humaine proviennent de ressources souterraines), irrigation, usages industriels (refroidissement, lavage, etc.) mais �galement la gestion des d�bits des cours d'eau \citep{defi2021, rodriguez2014predictive}. 

La pr�diction de ces niveaux repr�sente un v�ritable d�fi du fait de la complexit� des m�canismes hydrologiques � l'{\oe}uvre \citep{Bredy_2020}. De nombreux mod�les num�riques ont �t� d�velopp�s pour cette t�che. 
Certains mod�les sont bas�s sur une mod�lisation physique de la nappe phr�atique. Les mod�les physiques n�cessitent un ajustement des param�tres pour chaque situation \citep{nayak2006groundwater}. Elles offrent une solution pr�cise de pr�diction mais difficilement g�n�ralisable � grande �chelle. 
Pour les besoins de pr�diction de mani�re plus syst�matique, l'utilisation de s�ries temporelles historiques des niveaux a �t� vue depuis des ann�es comme un outil essentiel dans la planification des ressources en eau \citep{kisi2012forecasting}. De nombreux mod�les d'apprentissage automatique ont ainsi �t� d�velopp�s pour pr�dire le niveau des nappes phr�atiques. Par exemple, \cite{Bredy_2020} proposent une mod�lisation � l'aide de for�t al�atoire (RF) ou d'Extreme Gradient Boosting (XGB). \cite{Ahmedbahaaaldin2021} comparent �galement un mod�le bas� sur XGB mais �valuent �galement des mod�les SVR et des r�seaux de neurones. Dans ces deux travaux, les donn�es historiques utilis�es comprennent d'une part l'historique des niveaux de nappe, mais �galement des informations relatives aux pluies et � l'�vapotranspiration. Ces informations permettent de tenir compte des flux d'eau entrants et sortants dans une nappe. 

Parmi ces nombreux mod�les de l'�tat de l'art, il est difficile d'identifier celui qui est a priori le plus adapt� � la pr�diction de l'�volution du niveau des nappes phr�atiques tel que mesur� par le r�seau Hub'eau\footnote{Hub'eau~: \url{http://hubeau.eaufrance.fr/}. }. 
Il a donc nous sembl� int�ressant d'explorer des mod�les vari�s de pr�vision de l'�volution de s�ries temporelles pour les donn�es sp�cifiques mises � disposition et de les comparer pour identifier le meilleur.


Dans cet article nous nous sommes plus particuli�rement int�ress�s � la question suivante~: \og{}est-il pr�f�rable d'apprendre des mod�les par capteur (mod�les locaux) ou d'apprendre un mod�le global pour pr�voir l'�volution des niveaux pi�zom�triques~?\fg{}. En effet, les approches usuelles de pr�vision de s�ries temporelles sont con�ues pour apprendre � pr�dire, � partir des mesures pass�es d'un capteur, les mesures � venir de ce m�me capteur. Mais un mod�le appris sur chaque pi�zom�tre n�cessite d'avoir un historique de donn�es important pour chacun d'eux. Ce n'est pas le cas pour la mise en place de nouveaux capteurs par exemple et, plus g�n�ralement, cela rend le mod�le peu robuste aux changements du fonctionnement du syst�me hydrologique mesur�. L'utilisation de mod�les appris sur plusieurs pi�zom�tres peut permettre plus de robustesse et une mise en {\oe}uvre plus ais�e sur de nouveaux capteurs.

Notre exploration de mod�les de pr�vision de l'�volution de s�ries temporelles est donc structur�e par l'objectif de comparer trois types de mod�les~:
\begin{itemize}
\item des mod�les \textit{locaux} de pr�vision de s�rie temporelle qui sont appris ind�pendamment sur chaque s�rie temporelle (� partir des donn�es historiques) pour pr�dire son �volution future.
\item un mod�le \textit{global} de pr�vision unique appris � partir de l'ensemble des s�ries temporelles disponibles. Pour qu'un tel mod�le puisse tenir compte des conditions sp�cifiques de chaque r�gion, nous avons aussi pris en compte des informations sur la nature des sols qui influe la dynamique hydrologique.
\item un mod�le \textit{hybride} qui consiste � identifier des groupes de s�ries temporelles pour lesquels les dynamiques d'�volutions sont homog�nes. Un seul mod�le est construit pour chaque groupe de pi�zom�tre ainsi cr��.
\end{itemize}


Pour chacun de ces types de mod�les, nous avons propos� plusieurs impl�mentations possibles qui seront d�taill�es par la suite. Puis, pour s�lectionner le meilleur mod�le, nous avons compar� les RMSSE\footnote{RMSSE: Root Mean Scaled Square Error.} de ces diff�rents mod�les soit de mani�re globale, soit sp�cifiquement sur les 18 pi�zom�tres qui avaient �t� identifi�s pour le d�fi. 

%Dans la suite de cet article, nous pr�sentons tout d'abord les mat�riels et m�thodes. Dans la section suivante, nous pr�sentons les r�sultats qui ont �t� obtenus et nous les discutons. 

\section{Mat�riel et m�thodes}
Dans cette section, nous pr�sentons tout d'abord le jeu de donn�es que nous avons utilis�, ainsi que les pr�traitements qui ont �t� faits. 
Ensuite, nous pr�sentons les m�thodes de pr�dictions de s�ries temporelles qui ont �t� mises en {\oe}uvre. 

\subsection{Constitution des jeux de donn�es}\label{sec:methodo:dataset}
Pour ce travail, nous avons constitu� un jeu de donn�es correspondant � la p�riode de janvier 2015 � janvier 2021 (2221 jours) pour tous les pi�zom�tres de France m�tropolitaine pr�sentant des s�ries temporelles journali�res du niveau de nappe pour lesquelles il y a moins de 50 jours de donn�es manquantes. Les valeurs manquantes ont �t� imput�es par interpolation lin�aire. 
On obtient alors une base de donn�es de 1339 s�ries temporelles (i.e. pi�zom�tres). 
Parmi ces pi�zom�tres, on distingue le groupe de 18 s�ries temporelles auxquelles le d�fi s'int�resse plus particuli�rement. 

En compl�ment, nous avons �galement collect� avec des informations sur la nature du sol issues de la BD LISA\footnote{BD LISA: \url{https://bdlisa.eaufrance.fr/}} (th�me, nature, type de milieu et �tat), ainsi que deux s�ries temporelles journali�res sur la pluviom�trie et l'�vapotransporation\footnote{Donn�es issues du Climate Data Store~: \url{https://cds.climate.copernicus.eu}} (ETO) pris sur la maille spatiale du pi�zom�tre (mailles dont la taille est respectivement de 25$^o$ et 10$^o$).

Au final, pour chaque pi�zom�tre on dispose de trois s�ries temporelles journali�res (le niveau de nappe, la pluviom�trie et l'�vapotranspiration), ainsi que par quatre caract�ristiques de sol. 

\subsection{Pr�vision de s�ries temporelles}
La base de donn�es de pi�zom�tres est un ensemble de triplets $\mathcal{Y}=\langle {Y}^k, \bm{Z}^k, {F}^k\rangle$ o� ${Y}^k: y^k_{1\dots t}$ une s�rie temporelle univari�e telle que $y^k_i\in \mathbb{R}$ pour tout $i$, et $\bm{z}^k_{1..\infty}$ une s�rie temporelle multivari�e (${Z}: \bm{z}^k_i\in \mathbb{R}^m$), dite exog�ne, connue jusqu'� la date $t$ mais �galement au del�. La s�rie temporelle ${Y}^k$ est la s�rie du niveau de nappe tandis que la s�rie de donn�es exog�nes correspond � la pluviom�trie et � l'�vapotranspiration. 
${F}$ correspond aux informations disponibles sur le type de sol (4 informations).

La pr�diction d'une s�rie temporelle ${Y}^k $ � l'horizon $h$ consiste � estimer $y^k_{t+1 \dots t+h}$.
% 
Pour cette t�che de pr�vision de s�rie temporelle, l'approche classique en analyse de donn�es est de construire un mod�le autor�gressif. 
%
Une telle m�thode construit une fonction de pr�diction de la valeur � la date $t_0$ � partir des $r$ derni�res observations de ${Y}$ et de $\bm{Z}$, des valeurs de $\bm{Z}$ � la date $t_0$, et �ventuellement des caract�ristiques de la s�rie temporelle (${F}$). On d�note par
$\varphi : \mathbb{R}^{r\times(m+1)+f} \mapsto \mathbb{R}$ une telle fonction de pr�diction de la valeur suivante de la s�rie. $\varphi$ peut alors �tre vue comme un r�gresseur~: 
elle pr�dit une valeur r�elle � partir des caract�ristiques d'entr�e. 
%
Pour r�aliser une pr�vision � un horizon $h$, la fonction de pr�diction est r�cursivement appliqu�e $h$ fois.


On peut noter que les s�ries temporelles exog�nes sont suppos�es connues dans le futur. Ce probl�me est donc diff�rent de celui d'effectuer la pr�vision d'une s�rie temporelle multivari�e pour laquelle la fonction de pr�diction aurait pour sortie $m+1$ valeurs (la valeur de la s�rie cible ainsi que les $m$ valeurs des s�ries exog�nes). 
Dans notre probl�me, nous avons fait l'hypoth�se que la pluviom�trie et l'�vapotranspiration �taient semblables d'une ann�e � l'autre. Par cons�quent, les pr�visions peuvent �tre obtenues dans le futur en prenant les valeurs journali�res moyennes des ann�es pass�es. 
L'erreur d'approximation faite par cette hypoth�se nous semble pr�f�rable � celle qui serait faite par le cumul des erreurs d'une pr�diction r�cursive multivari�e � moyen terme. 
Pour conforter cette hypoth�se, la Section \ref{sec:res:approx} compare des r�sultats obtenus sur des donn�es historiques en utilisant l'approximation � ceux utilisant la valeur r�elle des s�ries exog�nes.


\subsection{Apprentissage des mod�les}\label{sec:methodo:learning}
Pour d�finir la m�thode d'apprentissage d'une fonction  $\varphi$ telle qu'introduite pr�c�demment, nous avons besoin d'une part de construire un jeu d'apprentissage et, d'autre part, de choisir un type de mod�le de r�gression.

Pour une s�rie temporelle $\langle {Y}^k, \bm{Z}^k, {F}^k\rangle$ dont on cherche � faire la pr�vision � partir de la date $t_0$ en utilisant $r$ valeurs dans le pass�, le jeu d'apprentissage de la fonction $\varphi$ est constitu� d'exemples $$(y_{t-r},\dots,y_{t},z^1_{t-r},\dots,z^m_{t},f_1,\dots,f_4), \forall t\in[r,t_0].$$
o� $y$ est la variable � pr�dire et les autres variables sont des variables explicatives.

Pour la construction du jeu d'apprentissage, nous avons consid�r� deux approches diff�rentes selon que l'on souhaite construire un mod�le local, global ou hybride. 
\begin{itemize}
\item un mod�le \textit{local} est construit � partir d'une s�rie temporelle et appliqu� sur cette m�me s�rie pour effectuer une pr�vision. Il y a donc un mod�le local par s�rie temporelle.
\item un mod�le \textit{global} est construit � partir de l'ensemble des s�ries temporelles et cet unique mod�le est appliqu� � toutes les s�ries dont on souhaite les pr�visions. 
\item un mod�le \textit{hybride} est construit pour chaque groupe homog�ne de s�ries temporelles. Pour une nouvelle s�rie, on applique le mod�le du groupe dont la s�rie est la plus proche. La section suivante d�taille comment les groupes homog�nes sont constitu�s. 
\end{itemize}

Pour le choix du type de r�gression, nous avons explor� les mod�les suivants~:
\begin{itemize}
\item R�gression lin�aire. C'est la m�thode standard pour les probl�mes de r�gression. Cette m�thode est alors �quivalente � celle qui consiste � utiliser un mod�le AR d'ordre $r$. 
\item SVR \citep{awad2015support}. Les SVR permettent de traiter des probl�mes en grande dimension et proposent des mod�les non-lin�aires. Nous avons utilis� des noyaux Gaussiens. Apr�s quelques exp�rimentations, nous avons fix� le param�tre $C=100$.
\item For�ts al�atoires \citep{breiman2001random}. C'est une m�thode d'apprentissage � ensemble (\textit{bagging}). Nous avons opt� pour une for�t de 100 arbres.
\item \textit{Extreme Gradient Boosting} \citep{chen2016xgboost}. C'est une m�thode � ensemble � base d'arbres qui utilise des techniques d'optimisation pour am�liorer l'efficacit� calculatoire de l'apprentissage du mod�le. Elle peut ainsi traiter de gros jeux de donn�es. 
\end{itemize}

En compl�ment de ces m�thodes d'apprentissage, nous avons �galement explor� le mod�le classique de pr�vision de s�ries temporelles ARIMA (\textit{Auto-Regressive Integrated Moving Average}) qui peut �tre vu comme un mod�le local dans notre classification.

\subsection{Construction de groupes homog�nes de s�ries temporelles} \label{sec:methodo:group}

L'approche hybride vise � construire un mod�le de pr�vision pour des groupes de s�ries temporelles. On souhaite pour cela utiliser une repr�sentation des s�ries temporelles qui capte une information sur la dynamique de l'�volution de ces s�ries.
Pour cela, on utilise les caract�ristiques suivantes~:
\begin{enumerate}
\item la valeur des param�tres d'un mod�le ARMA d'ordre 3 estim� sur la s�rie temporelle $Y$ avant $t_0$. 
Ces param�tres servent � repr�senter la dynamique d'�volution d'une s�rie temporelle.
\item les moyennes et �carts types de la s�rie temporelle $Y$ avant $t_0$ et 
\item les descriptions du sol issues des 4 param�tres de la BD LISA ($f_1,\dots,f_4$).
\end{enumerate} 

Les attributs cat�goriels ont �t� vectoris�s de sorte qu'un pi�zom�tre soit repr�sent� par un vecteur r�el de dimension $26$. 
Chacun de ces attributs est $z$-normalis� pour leur donner une �gale importance. 
Les donn�es sont cat�goris�es par l'algorithme des $k$-\textit{means}. 
Les valeurs de $k$ sont test�es entre 2 et 15, et le choix du meilleur $k$ est fait selon le crit�re BIC.

On peut noter que l'approche globale est un cas particulier de l'approche hybride o� toutes les s�ries sont dans le m�me cluster ($k=1$). 

\subsection{Comparaison des mod�les}\label{sec:methodo:cmp}

Pour le choix de la meilleure m�thode � adopter pour effectuer des pr�visions, nous comparons les approches en utilisant les RMSSE pour chaque m�thode et chaque s�rie pour un horizon de trois mois ($h=93$)~:
$$RMSSE=\sqrt{ \frac{\frac{1}{h}\sum_{t=n+1}^{n+h}\left(y_t-\hat{y_t}\right)^2}{\frac{1}{n-1}\sum_{t=2}^{n}\left(y_t-y_{t-1}\right)^2}}$$

Plus pr�cis�ment, le calcul des RMSSE est fait uniquement sur la p�riode du 15 octobre jusqu'au 15 janvier 2020. En effet, en fonction de la p�riode de l'ann�e, la pr�diction est plus ou moins ais�e (en p�riode s�che, les nappes phr�atiques sont moins impact�es par les pluies par exemple). On a donc choisi d'�valuer nos mod�les sur la p�riode de l'ann�e que le d�fi imposait.

� partir du calcul de l'ensemble des RMSSE, nous calculons les moyennes des RMSSE sur l'ensemble des s�ries et plus sp�cifiquement sur les 18 s�ries � pr�dire (la Section \ref{sec:expe} ne pr�sente les r�sultats que pour l'ensemble des s�ries, plus de r�sultats sont disponibles dans le d�p�t logiciel du projet). 
En compl�ment, on r�alise �galement des comparaisons en s'appuyant sur des diagrammes de diff�rences critiques \citep{demsar2006statistical}. Ces diagrammes s'appuient sur des statistiques de rang (test de Nemenyi) pour comparer des m�thodes.


\section{Exp�rimentations et r�sultats}\label{sec:expe}

L'ensemble des mod�les ont �t� mis en {\oe}uvre et test�s � l'aide du logiciel \texttt{R}.\footnote{L'ensemble des d�veloppements du projet peut �tre consult� sur le site \url{https://gitlab.inria.fr/tguyet/projetpiezo}.} Pour les mod�les ARIMA, for�ts al�atoires, SVR et XGBoost, nous avons respectivement utilis� les librairies \texttt{forecast}, \texttt{randomForest}, \texttt{e1071} et \texttt{XGBoost}.

Dans une premier temps, nous nous int�ressons au choix de la taille de l'historique.
Nous d�taillons ensuite les r�sultats obtenus avec les mod�les locaux, puis la Section \ref{sec:result:glohyb}, nous donnons les r�sultats obtenus avec les approches globales et hybrides.

\subsection{Choix de la taille de l'historique}
Le choix de la taille de l'historique (param�tre $r$) doit tenir compte 1) de l'horizon de pr�vision, 2) des caract�ristiques de la s�rie (notamment des corr�lations entre la s�rie � pr�dire et les s�ries exog�nes) et 3) de la dimension des exemples d'apprentissage.
Les analyses de corr�lation crois�e entre les s�ries exog�nes et la s�rie � pr�dire montre une forte corr�lation pour un d�calage de $100$ jours~: ceci laisse penser que disposer de l'information d'au moins 100 jours ($r> 100$) dans le pass� aidera un r�gresseur � faire des pr�visions pr�cises. 
L'horizon de pr�vision est fix� � $h=93$. L'utilisation d'une proc�dure r�cursive de pr�vision pas � pas incite � utiliser une taille d'historique $r> h$ de sorte qu'aucune pr�diction ne se fasse sans s'appuyer sur aucune donn�e r�elle (\ie pas uniquement � partir de donn�es elles-m�mes pr�alablement pr�dites). Ces deux contraintes incitent donc � avoir un $r> 100$. N�anmoins, une valeur trop �lev� de $r$ conduira � des exemples en tr�s grande dimension, limitant les performances de certaines m�thodes d'apprentissage. 

\begin{figure}[tbp]
\centering
\includegraphics[width=.6\textwidth]{figures/expe_r}
\caption{RMSSE moyennes sur les 18 pi�zom�tres � tester en fonction de la taille d'historique.}
\label{fig:expe_r}
\end{figure}

Pour finaliser notre d�cision, nous avons men� une �tude exp�rimentale sur les 18 pi�zom�tres de test en testant des valeurs de $r$ de 40 � 140 pour le mod�le local de r�gression lin�aire. Les r�sultats de cette exp�rimentation sont illustr�s dans la Figure \ref{fig:expe_r}. On observe sur cette figure que les meilleurs r�sultats m�dians sont obtenus pour $r=100$ ou $110$. Pour limiter la dimensionnalit� des exemples, nous avons choisi $r=100$ pour la suite des exp�rimentations.


\subsection{Analyse des r�sultats des mod�les locaux}

\paragraph{Comparaison des mod�les individuels}
On commence par comparer les r�sultats obtenus avec les diff�rents r�gresseurs sur l'ensemble des pi�zom�tres avec $r=50$ et $r=100$.  

La Figure \ref{fig:cmp_ml} illustre les r�sultats obtenus sous la forme d'un graphique et d'un diagramme de diff�rences critiques (pour $r=100$ uniquement). 

Ces r�sultats montrent d'une part que l'utilisation d'un historique de 100 est meilleur qu'un historique de 50 pour tous les r�gresseurs. D'autre part, on identifie deux r�gresseurs comme potentiellement plus pr�cis que les autres~: la r�gression lin�aire et les for�ts al�atoires (non statistiquement diff�rents). Le mod�les ARIMA et SVR donnent des RMSSE qui sont significativement sup�rieures aux autres (selon le test de Nemenyi). 

\begin{figure}[tbp]
\centering
\includegraphics[width=.48\textwidth]{figures/expe_cmp_ml}
\includegraphics[width=.48\textwidth]{figures/cd_cmp_ml}
\caption{Sur la gauche~: RMSSE moyennes sur l'ensemble des pi�zom�tres � tester en fonction des types de r�gresseur. On compare deux tailles d'historique~: $r=5�$ (en rouge) et $r=100$ (en bleu). Le mod�le ARIMA n'ayant pas de param�tre $r$, seule une valeur est report�e. Sur la droite~: diagramme de diff�rence critique comparant les diff�rentes m�thodes sur tous les pi�zom�tres ($r=100$).}
\label{fig:cmp_ml}
\end{figure}

\paragraph{Apport des s�ries exog�nes}\label{sec:res:apport_exo}
On cherche maintenant � montrer l'apport de l'utilisation des donn�es exog�nes pour am�liorer la pr�cision des pr�dictions. 
Pour cela, on compare les r�sultats de RMSSE de diff�rents mod�les dans trois configurations~: pr�diction sans donn�es exog�nes, avec la pluie seule, avec l'�vapotransporation (ETO) seule et avec la pluie et l'ETO. 
Dans ce cas, on a pris $r=100$. 
On se place ici la situation id�ale o� on conna�t les s�ries exog�nes r�elles. 

\begin{figure}[tbp]
\centering
\includegraphics[width=.55\textwidth]{figures/expe_cmp_exo}
\caption{RMSSE moyennes sur l'ensemble des pi�zom�tres en fonction de la taille d'historique.}
\label{fig:cmp_exo}
\end{figure}

La Figure \ref{fig:cmp_exo} illustre les r�sultats obtenus sur l'ensemble des pi�zom�tres. On constate que l'utilisation de s�ries exog�nes diminue fortement les performances des mod�les. L'utilisation de l'information combin�e de pluie et �vapotranspiration limite cette diminution, mais ne permet pas d'am�lioration. Une explication de ces mauvais r�sultats est l'augmentation de la dimension des exemples qui rend l'apprentissage plus difficile (plus de 300 caract�ristiques avec les deux variables exog�nes). Les mod�les comme les for�ts al�atoires et les SVR se montrent plus performants pour ce type de donn�es.

\paragraph{Perte de pr�cision li�e � l'utilisation de donn�es exog�nes approxim�es}\label{sec:res:approx}
Finalement, bien que l'ajout de donn�es exog�nes ne se montre pas utile, nous avons compar� les r�sultats obtenus en utilisant des donn�es exog�nes r�elles ou bien approxim�es (valeurs moyennes dans le pass�). Ces derni�res correspondent mieux aux contraintes pratiques d'utilisation. 

La Figure \ref{fig:cmp_wwexo} illustre les r�sultats de RMSSE avec ou sans approximation des donn�es exog�nes. On constate que quelque soit le r�gresseur, les performances sont tr�s semblables avec ou sans approximation. Ce r�sultat est donc int�ressant~: il est probable que si une m�thode exploite bien les donn�es exog�nes, elle puisse indiff�remment utiliser les donn�es approxim�es � la place des donn�es r�elles.

\begin{figure}[tbp]
\centering
\includegraphics[width=.55\textwidth]{figures/expe_cmp_wwexo}
\caption{RMSSE moyennes sur l'ensemble des pi�zom�tres avec ou sans approximation des variables exog�nes.}
\label{fig:cmp_wwexo}
\end{figure}

\subsection{Analyse des r�sultats des approches hybride ou globale}\label{sec:result:glohyb}
Dans cette partie, on s'int�resse aux r�sultats obtenus par les approches globale et hybride. 
Pour des raisons de ressources de calcul, seuls les mod�les bas�s sur la r�gression lin�aire et XGB ont pu �tre calcul�s.\footnote{Les donn�es pour l'ensemble des pi�zom�tres avec un historique de $r=100$ n�cessite environ 20Go de m�moire RAM. L'utilisation de serveurs de calculs disposant de 32Go de RAM ne permettant pas de contenir les informations n�cessaires pour les mod�les d'arbre de d�cision ou de SVR.} 
Nous nous sommes �galement limit� aux configurations $r=100$ et sans approximation des valeurs exog�nes. 
Pour les comparaisons avec les mod�les locaux, on ne prendra donc que ces configurations en compte.

Pour l'approche hybride, le meilleur nombre de cluster a �t� estim� � 8 selon le crit�re BIC. On obtient ainsi 8 groupes de s�ries temporelles de tailles $426$, $266$, $181$, $132$, $106$, $65$, $11$ et $5$.

Pour l'approche globale, on utilise l'information des caract�ristiques des sols (BD LISA) comme des variables explicatives pour la r�gression. L'objectif est de voir si il �tait pr�f�rable d'utiliser cette information pour cat�goriser les s�ries temporelles ou bien de l'utiliser directement dans la r�gression.


\begin{figure}[tbp]
\centering
\includegraphics[width=1\textwidth]{figures/expe_cmp_exo_hybglobloc}
\caption{Comparaison des RMSSE en fonction de l'utilisation des donn�es exog�nes, selon le type d'approche (globale, hybride ou locale) et selon le classifieur (XGBoost, \texttt{xgb} ou r�gression lin�aire, \texttt{lm}).
}
\label{fig:results_global}
\end{figure}

La Figure \ref{fig:results_global} donne les r�sultats obtenus pour les diff�rentes approches en fonction de l'utilisation de diff�rents jeux de caract�ristiques exog�nes dans la r�gression (pluie, pluie+ETO, BD LISA, pluie+ETO+BD LISA). Pour les mod�les hybride et global, on constate que quelles que soient les donn�es exog�nes disponibles, les performances sont semblables. 
En particulier, l'utilisation de l'information sur le sol (BD LISA) ne permet pas de r�duire les RMSSE. 
En comparaison avec le mod�le local pour lequel l'utilisation d'information exog�ne �tait p�nalisante (en particulier avec XGBoost), les mod�les utilisant plusieurs pi�zom�tres sont plus robustes � l'utilisation de nouvelles caract�ristiques. Ceci est probablement d� au plus grand nombre de donn�es disponibles pour ajuster les param�tres additionnels.


\begin{figure}[tbp]
\centering
\includegraphics[width=.9\textwidth]{figures/expe_cmp_type_hybglobloc}
\caption{Comparaison des RMSSE en fonction de l'utilisation du type d'approche (globale, hybride ou locale) pour la r�gression lin�aire.}
\label{fig:results_type}
\end{figure}

La Figure \ref{fig:results_type} compare les RMSSE obtenues pour les diff�rentes approches avec la r�gression lin�aire. Elle est d�taill�e pour chaque sous jeu de caract�ristiques qui ont �t� exp�riment�s en commun. 
Cette figure illustre le fait qu'en moyenne, les approches globale et hybride ont des pr�cisions semblables. En revanche, avec l'utilisation de donn�es exog�nes de pluie et d'�vapotranspiration, ou sans donn�es exog�nes, on constate que l'approche locale a une meilleure pr�cision.


Pour conforter ce r�sultat obtenu sur des agr�gats de RMSSE, la Figure \ref{fig:cd_cmp_typeexo} donne le diagramme de diff�rences critiques obtenus en comparant pair � pair les RMSSE pour chaque couple~: type d'approche et jeu de caract�ristiques exog�nes. On a ici fix� l'utilisation de la r�gression lin�aire, que nous avons vu pr�c�demment comme semblant �tre la meilleure approche. 


\begin{figure}[tbp]
\centering
\includegraphics[width=\textwidth]{figures/cd_cmp_typeexo}
\caption{Diagramme de diff�rences critiques comparant les RMSSE en fonction de l'utilisation du type d'approche (globale, hybride ou locale) avec diff�rents jeux de caract�ristiques exog�nes. 
La m�thode d'apprentissage utilis�e est ici la r�gression lin�aire.}
\label{fig:cd_cmp_typeexo}
\end{figure}

Ce diagramme montre clairement la sup�riorit� de l'approche locale, sans donn�es exog�nes. Son rang moyen est proche de 3 (sur 11 m�thodes compar�es). Elle est donc tr�s r�guli�rement class�e parmi les toutes meilleures approches. 
Le diagramme montre que cette diff�rence est significative statistiquement. 
�tonnamment, le moins bon mod�le est obtenu avec une configuration proche~: l'approche locale avec l'utilisation de l'information de pluie. Comme on l'a vu pr�c�demment, cela s'explique probablement par le manque de donn�es suffisantes pour ajuster les nombreux param�tres induits par l'utilisation de cette nouvelle information. 
Les autres approches offrent globalement des performances similaires entre elles (classements moyens entre 5 et 7).


\subsection{Comparaison sur le jeu entier contre les 18 s�ries de test}
Nous concluons cette partie exp�rimentale en comparant les r�sultats obtenus sur les 18 s�ries temporelles de test par rapport aux r�sultats pr�c�dents, obtenus pour l'ensemble du jeu de donn�es. En effet, il faut v�rifier que le choix du meilleur mod�le sur la base de l'ensemble du jeu de donn�es sera potentiellement le meilleur �galement pour le sous ensemble de donn�es qui sera utilis� pour le d�fi.

La Figure \ref{fig:expe_cmp_lm18all} illustre les diff�rences de RMSSE obtenues pour l'approche locale sans variable exog�ne. On constate que lorsqu'on s'int�resse aux 18 pi�zom�tres � pr�dire, les r�sultats sont du m�me ordre que pour toute la base, voire l�g�rement inf�rieure, quelque soit le r�gresseur utilis�. Le d�fi a donc retenu des s�ries temporelles plut�t plus faciles � pr�dire que le reste de la base de donn�es. Le meilleur r�gresseur reste la r�gression lin�aire pour ce sous-ensemble.

\begin{figure}[tbp]
\centering
\includegraphics[width=.9\textwidth]{figures/expe_cmp_lm18all}
\caption{Comparaison des RMSSE pour la r�gression lin�aire des 18 pi�zom�tres � tester (� droite, en blanc) contre tous les pi�zom�tres (� gauche, en gris).}
\label{fig:expe_cmp_lm18all}
\end{figure}


\section{Conclusion}
En conclusion, notre approche du d�fi a consist� � exp�rimenter diff�rentes approches de pr�diction de s�ries temporelles en se basant sur une technique d'autor�gression~: les donn�es historiques de la s�rie temporelle permettent d'apprendre un r�gresseur pour pr�dire la valeur suivante de la s�rie. La pr�vision de la s�rie est men�e en appliquant r�cursivement le r�gresseur appris.

Pour le choix et l'apprentissage du r�gresseur, nous avons mis en place un protocole pour identifier le meilleur choix. Nous avons compar� quatre types de classifieur (lin�aires, SVR, for�ts al�atoires et XGB) et exp�riment� la construction d'une base d'apprentissage de trois mani�res~: locale, globale et hybride. Au travers de l'approche hybride, nous souhaitions am�liorer la pr�cision des pr�visions en cr�ant des sous groupes homog�nes de s�ries temporelles. Finalement, nous avons �galement exp�riment� l'utilisation de variables exog�nes (pluie, �vapotranspiration et caract�ristiques de sol).

En pratique, des exp�rimentations extensives ont �t� men�es sur les donn�es afin d'identifier la solution que nous emploierons pour r�pondre au d�fi. La solution qui est retenue est finalement la plus simple~: elle consiste faire un mod�le de r�gression par s�rie temporelle avec un historique de 100 valeurs, et sans variables exog�nes.

\vspace{10pt}

Le code mis en place est un code flexible et facilement adaptable � l'utilisation et l'exp�rimentation de nouveaux r�gresseurs. Une premi�re perspective de ce travail serait donc d'affiner l'utilisation des r�gresseurs les plus avanc�s (notamment XGBoost) en ajustant leurs hyper-param�tres de sorte � en tirer les meilleures performances. 

Une seconde am�lioration possible serait d'am�liorer la construction des groupes de s�ries temporelles dans l'approche hybride. L'absence de diff�rence avec l'approche globale laisse penser que la constitution des groupes n'identifie pas les bonnes caract�ristiques pour faciliter l'apprentissage d'un bon mod�le commun. Plus d'exp�rimentations pourraient �tre men�es de ce c�t�.

Finalement, nous avons conduit ici une analyse purement guid�e par les donn�es. La litt�rature montre n�anmoins qu'une connaissance applicative peut donner de meilleurs r�sultats, notamment en tenant compte des d�lais d'impact entre les variables, ou leur dur�e d'int�gration. L'utilisation de connaissance expertes devrait donc permettre d'am�liorer les mod�les.

\bibliographystyle{rnti}
\bibliography{biblio}

\Fr

\end{document}
