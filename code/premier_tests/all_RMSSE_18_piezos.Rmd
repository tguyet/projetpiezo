---
title: "RMSSE_par_modèle_18_piezo"
output: html_document
---

library
```{r}
library(tidyverse)
library(tsibble)
library(e1071)
library(lubridate)
library(lmtest)
library(forecast)
```

```{r}
rm(list=ls()) 
data_avec_na <-  read_delim('../../data/dataset_18_2015_2020.csv', ",")
code_bss <-data_avec_na$bss %>% unique() 
# il y a plusieurs données manquantes pour les données de niveau d'eau, on fait donc une imputation de données manquantes
data <- data.frame()
for(i in 1:length(code_bss)){
  bss <- data_avec_na %>% filter(bss==code_bss[i])
  bss_ts <- as_tsibble(bss,key = NULL, index=time)
  bss_ts$imputed <- na.interp(bss_ts$p)
  data <- rbind(data,data.frame(bss=code_bss[i],t=bss_ts$time,e=bss_ts$e,tp=bss_ts$tp,p=bss_ts$imputed))
}

```

approximation de tp et e

```{r}
# h=93
# data_moy_e_tp <- data.frame()
# for (i in 1:length(code_bss)){
#   bss <- data %>% filter(bss ==code_bss[i])
#   bss_train <- bss[1:(nrow(bss) - h),]
#   bss_train$day <- as.numeric(format(bss_train$t,format="%d"))
#   bss_train$month <- as.numeric(format(bss_train$t, format = "%m"))
#   bss_train$daymonth <- ymd(paste0('2020-',bss_train$month,'-',bss_train$day))
#   
#   
#   moy_day_e_tp <- bss_train %>% group_by(daymonth) %>% dplyr::summarise(e = mean(e, na.rm = TRUE),tp = mean(tp, na.rm = TRUE), .groups='drop') %>% ungroup() %>% select("daymonth","e","tp")
#   
#   test_p <- bss[(nrow(bss)-h+1):nrow(bss),]$p
#   test_moy_e_tp <- moy_day_e_tp[(nrow(moy_day_e_tp)-h+1):nrow(moy_day_e_tp),]
#   data_moy_e_tp <- rbind(data_moy_e_tp,rbind(data.frame(bss=code_bss[i],t=bss_train$t,e=bss_train$e,tp=bss_train$tp,p=bss_train$p),data.frame(bss=code_bss[i],t=test_moy_e_tp$daymonth,e=test_moy_e_tp$e,tp=test_moy_e_tp$tp,p=test_p)))
# }
# 
# data <- data_moy_e_tp

```

fonctions
```{r}
fct_matrice <- function(train_df,k){
  vect_p <- train_df[-(1:k),]$p
  vect_tp <- train_df[-(1:k),]$tp
  vect_e <- train_df[-(1:k),]$e
  v <- tibble()
  w <- tibble()
  x <- tibble()
  n <- nrow(train_df)
  
  for (j in 1:(k-1)){
    vect_p_i <-train_df[-c(1:(k-j),(n-j+1):n),]$p
    vect_tp_i <-train_df[-c(1:(k-j),(n-j+1):n),]$tp
    vect_tp_i <-train_df[-c(1:(k-j),(n-j+1):n),]$e
    v <- rbind( vect_p_i, v)
    w <- rbind( vect_tp_i, w)
    x <- rbind( vect_tp_i, x)
  } 
  vect_p_k <- train_df[-((n-k+1):n),]$p 
  vect_tp_k <- train_df[-((n-k+1):n),]$tp
  vect_e_k <- train_df[-((n-k+1):n),]$e
  matrice <- cbind(data.frame(vect_e_k),t(x),data_frame(vect_e),data.frame(vect_tp_k),t(w),data_frame(vect_tp),data.frame(vect_p_k),t(v),data_frame(vect_p))
  colnames(matrice) <- c(paste("e_",seq(k,0,-1), sep=""),paste("tp_",seq(k,0,-1), sep=""),paste("p_",seq(k,0,-1), sep=""))
  rownames(matrice) <- 1:(n-k)
  matrice
} 

#Fonction de Simon pour définir un modèle de regression à partir d'une liste d'indices.
myreg = function(data, idxp, idxc){
  if(length(idxp)==0){
    fff = as.formula(paste(colnames(data)[idxc],"~1", sep = ""))
  } else{
    fff = as.formula(paste(paste(colnames(data)[idxc],"~"),paste(colnames(data)[idxp], collapse = "+")))
  }
  return(lm(fff, data = data))
}


modele_svm = function(data, idxp, idxc){
 
  if(length(idxp)==0){
    fff = as.formula(paste(colnames(data)[idxc],"~1", sep = ""))
  }
  else{
  fff = as.formula(paste(paste(colnames(data)[idxc],"~"),paste(colnames(data)[idxp],
                                                               collapse = "+")))
  }
  return(svm(fff, data = data,type='eps-regression',kernel='radial'))
}
```


```{r}
h=93

#choix de k 
k=100



rmsse_arima_p <- rep(NA)
rmsse_arima_p_tp <- rep(NA)
rmsse_lm_p <- rep(NA)
rmsse_lm_p_tp <- rep(NA)
rmsse_lm_p_tp_e <- rep(NA)
rmsse_svr_p <- rep(NA)
rmsse_svr_p_tp <- rep(NA)
rmsse_svr_p_tp_e <- rep(NA)
rmsse_prophet <- rep(NA)

for ( j in 1:length(code_bss)){
  data_bss <-  data %>% filter(bss ==code_bss[j]) %>% select(p,tp,e,bss)
  train <-  data_bss[2:(nrow(data_bss) - h),]
  train$p_1 <-data_bss[1:(nrow(data_bss) - (h+1)),]$p
  train$Tn <- (train$p-train$p_1)^2
  TN=sum(train$Tn)/(nrow(data_bss)-1)
  test <- data_bss[(nrow(data_bss)-h+1):(nrow(data_bss)),]
  #on creer la matrice qui nous permettras d'effectuer les modèle lm et svr
  df <- fct_matrice(train,k)
  #on effectue le modèles qui nous permettrons d'effectuer les prédictions pour lm et svr
  lm_p_tp_e <- myreg(df,1:((k*3)+2),(k*3+3))
  lm_p_tp <- myreg(df,(k+2):(k*3+2),(k*3+3))
  lm_p <- myreg(df,(k*2+3):(k*3+2),(k*3+3))
  svr_p_tp_e <- modele_svm(df,1:((k*3)+2),(k*3+3))
  svr_p_tp <- modele_svm(df,(k+2):(k*3+2),(k*3+3))
  svr_p <- modele_svm(df,(k*2+3):(k*3+2),(k*3+3))
  df_test <- tibble()
  #préparation du jeu de donnée pour les prédiction de lm et svr
  data_bss_pred = data %>% filter(bss == code_bss[j]) %>% select(p,tp,e,bss)
  if (nrow(data_bss_pred) - h -k <1) {
    print(bss)
    next
  }
  data_bss_pred = data_bss_pred[(nrow(data_bss_pred) - h -k):nrow(data_bss_pred),] 
  data_bss_pred$gt = data_bss_pred[,1]
  data_bss_pred[(nrow(data_bss_pred) - h):nrow(data_bss_pred),1] <- NA
  df_test <- rbind(df_test, data_bss_pred)
  
  #prediction avec lm_p
  
  
  df_test_lm_p <- df_test
  for (i in (k+1):(93+k+1)){
    data_to_pred_lm_p <- rep(NA)
    for ( w in 1:k){
      data_to_pred_lm_p <- cbind(data_to_pred_lm_p, df_test_lm_p[i-w,1]) 
    }
    data_to_pred_lm_p <- t(data.frame(data_to_pred_lm_p[,-1]))
    colnames(data_to_pred_lm_p) <- paste('p_',1:k,sep='')
    data_to_pred_lm <- data.frame(data_to_pred_lm_p)
    df_test_lm_p[i,1] <-  predict.lm(lm_p,data_to_pred_lm)
  }
  df_test_lm_p_93 <- df_test_lm_p[-(1:(k+1)),]
  #prediction avec lm_p_tp
  df_test_lm_p_tp <- df_test
  for (i in (k+1):(93+k+1)){
    data_to_pred_lm_tp <- rep(NA)
    data_to_pred_lm_p <- rep(NA)
    for (v in 0:k){
      data_to_pred_lm_tp <- cbind(data_to_pred_lm_tp, df_test_lm_p_tp[i-v,2])
    }
    data_to_pred_lm_tp <- t(data.frame(data_to_pred_lm_tp[,-1]))
    colnames(data_to_pred_lm_tp) <- paste('tp_',0:k,sep='')
    for ( w in 1:k){
      data_to_pred_lm_p <- cbind(data_to_pred_lm_p, df_test_lm_p_tp[i-w,1]) 
    }
    data_to_pred_lm_p <- t(data.frame(data_to_pred_lm_p[,-1]))
    colnames(data_to_pred_lm_p) <- paste('p_',1:k,sep='')
    data_to_pred_lm <- data.frame(data_to_pred_lm_tp,data_to_pred_lm_p)
    df_test_lm_p_tp[i,1] <-  predict.lm(lm_p_tp,data_to_pred_lm)
  }
  df_test_lm_p_tp_93 <- df_test_lm_p_tp[-(1:(k+1)),]
  # prediction avec lm_p_tp_e
  df_test_lm_p_tp_e <- df_test
  for (i in (k+1):(93+k+1)){
    data_to_pred_lm_tp <- rep(NA)
    data_to_pred_lm_e <- rep(NA)
    data_to_pred_lm_p <- rep(NA)
    for (v in 0:k){
      data_to_pred_lm_tp <- cbind(data_to_pred_lm_tp, df_test_lm_p_tp_e[i-v,2])
    }
    data_to_pred_lm_tp <- t(data.frame(data_to_pred_lm_tp[,-1]))
    colnames(data_to_pred_lm_tp) <- paste('tp_',0:k,sep='')
    for (x in 0:k){
      data_to_pred_lm_e <- cbind(data_to_pred_lm_e, df_test_lm_p_tp_e[i-v,3])
    }
    data_to_pred_lm_e <- t(data.frame(data_to_pred_lm_e[,-1]))
    colnames(data_to_pred_lm_e) <- paste('e_',0:k,sep='')
    
    for ( w in 1:k){
      data_to_pred_lm_p <- cbind(data_to_pred_lm_p, df_test_lm_p_tp_e[i-w,1]) 
    }
    data_to_pred_lm_p <- t(data.frame(data_to_pred_lm_p[,-1]))
    colnames(data_to_pred_lm_p) <- paste('p_',1:k,sep='')
    data_to_pred_lm <- data.frame(data_to_pred_lm_e,data_to_pred_lm_tp,data_to_pred_lm_p)
    df_test_lm_p_tp_e[i,1] <-  predict.lm(lm_p_tp_e,data_to_pred_lm)
  }
  df_test_lm_p_tp_e_93 <- df_test_lm_p_tp_e[-(1:(k+1)),]
  # prediction avec svr_p
  df_test_svr_p <- df_test
  for (i in (k+1):(93+k+1)){
    data_to_pred_svr_p <- rep(NA)
    for ( w in 1:k){
      data_to_pred_svr_p <- cbind(data_to_pred_svr_p, df_test_svr_p[i-w,1]) 
    }
    data_to_pred_svr_p <- t(data.frame(data_to_pred_svr_p[,-1]))
    colnames(data_to_pred_svr_p) <- paste('p_',1:k,sep='')
    data_to_pred_svr <- data.frame(data_to_pred_svr_p)
    df_test_svr_p[i,1] <-  predict(svr_p,data_to_pred_svr)
  }
  df_test_svr_p_93 <- df_test_svr_p[-(1:(k+1)),]
  #prediction avec svr_p_tp
  df_test_svr_p_tp <- df_test
  for (i in (k+1):(93+k+1)){
    data_to_pred_svr_tp <- rep(NA)
    data_to_pred_svr_p <- rep(NA)
    for (v in 0:k){
      data_to_pred_svr_tp <- cbind(data_to_pred_svr_tp, df_test_svr_p_tp[i-v,2])
    }
    data_to_pred_svr_tp <- t(data.frame(data_to_pred_svr_tp[,-1]))
    colnames(data_to_pred_svr_tp) <- paste('tp_',0:k,sep='')
    for ( w in 1:k){
      data_to_pred_svr_p <- cbind(data_to_pred_svr_p, df_test_svr_p_tp[i-w,1]) 
    }
    data_to_pred_svr_p <- t(data.frame(data_to_pred_svr_p[,-1]))
    colnames(data_to_pred_svr_p) <- paste('p_',1:k,sep='')
    data_to_pred_svr <- data.frame(data_to_pred_svr_tp,data_to_pred_svr_p)
    df_test_svr_p_tp[i,1] <-  predict(svr_p_tp,data_to_pred_svr)
  }
  df_test_svr_p_tp_93 <- df_test_svr_p_tp[-(1:(k+1)),]
  #prediction avec svr_p_tp_e
  df_test_svr_p_tp_e <- df_test
  for (i in (k+1):(93+k+1)){
    data_to_pred_svr_tp <- rep(NA)
    data_to_pred_svr_e <- rep(NA)
    data_to_pred_svr_p <- rep(NA)
    for (v in 0:k){
      data_to_pred_svr_tp <- cbind(data_to_pred_svr_tp, df_test_svr_p_tp_e[i-v,2])
    }
    data_to_pred_svr_tp <- t(data.frame(data_to_pred_svr_tp[,-1]))
    colnames(data_to_pred_svr_tp) <- paste('tp_',0:k,sep='')
    for (x in 0:k){
      data_to_pred_svr_e <- cbind(data_to_pred_svr_e, df_test_svr_p_tp_e[i-v,3])
    }
    data_to_pred_svr_e <- t(data.frame(data_to_pred_svr_e[,-1]))
    colnames(data_to_pred_svr_e) <- paste('e_',0:k,sep='')
    
    for ( w in 1:k){
      data_to_pred_svr_p <- cbind(data_to_pred_svr_p, df_test_svr_p_tp_e[i-w,1]) 
    }
    data_to_pred_svr_p <- t(data.frame(data_to_pred_svr_p[,-1]))
    colnames(data_to_pred_svr_p) <- paste('p_',1:k,sep='')
    data_to_pred_svr <- data.frame(data_to_pred_svr_e,data_to_pred_svr_tp,data_to_pred_svr_p)
    df_test_svr_p_tp_e[i,1] <-  predict(svr_p_tp_e,data_to_pred_svr)
  }
  df_test_svr_p_tp_e_93 <- df_test_svr_p_tp_e[-(1:(k+1)),]

   #ARIMA
  
  # prediction arima avec p 
  fit_p <- auto.arima(train$p)
  fcast_p <- forecast(fit_p, h =h)
  all_fcast_p <- data.frame(prev=as.vector(fcast_p[["mean"]]))
  #prediction ariam avec p et tp 
  pluv_vect_prev <- as.vector(test$tp)
  fit_p_tp <- auto.arima(train$p, xreg = train$tp)
  fcast_tp <- pluv_vect_prev
  fcast_p_tp <- forecast(fit_p_tp, xreg = fcast_tp, h = h)
  all_fcast_p_tp <- data.frame(prev=as.vector(fcast_p_tp[["mean"]]))
 
  
  # prédiction avec prophet
  data_bss <-  data %>% filter(bss ==code_bss[j]) %>% select(t,p)
  test <- data_bss[(nrow(data_bss) - h+1):(nrow(data_bss)),]
  train_proph <-  data_bss[2:(nrow(data_bss) - h),]
  names(train_proph) <- c('ds', 'y') 
  m <- prophet(train_proph)
  future <- make_future_dataframe(m, periods=h)
  proph <- predict(m, future)
  prev_proph <- proph[(nrow(proph) - h+1):(nrow(proph)),]
  

  
  #rmsse
  rmsse_arima_p[j] <- sqrt((mean((as.vector(test$p)-as.vector(all_fcast_p$prev))^2))/TN)
  rmsse_arima_p_tp[j] <- sqrt((mean((as.vector(test$p)-as.vector(all_fcast_p_tp$prev))^2))/TN)
  
  rmsse_lm_p[j] <-  sqrt((mean((as.vector(df_test_lm_p_93$p)-as.vector(df_test_lm_p_93$gt))^2))/TN)
  rmsse_lm_p_tp[j] <-  sqrt((mean((as.vector(df_test_lm_p_tp_93$p)-as.vector(df_test_lm_p_tp_93$gt))^2))/TN)
  rmsse_lm_p_tp_e[j] <-  sqrt((mean((as.vector(df_test_lm_p_tp_e_93$p)-as.vector(df_test_lm_p_tp_e_93$gt))^2))/TN)
  
  rmsse_svr_p[j] <-  sqrt((mean((as.vector(df_test_svr_p_93$p)-as.vector(df_test_svr_p_93$gt))^2))/TN)
  rmsse_svr_p_tp[j] <-  sqrt((mean((as.vector(df_test_svr_p_tp_93$p)-as.vector(df_test_svr_p_tp_93$gt))^2))/TN)
  rmsse_svr_p_tp_e[j] <-  sqrt((mean((as.vector(df_test_svr_p_tp_e_93$p)-as.vector(df_test_svr_p_tp_e_93$gt))^2))/TN)
  
  rmsse_prophet[j] <- sqrt((mean((as.vector(test$p)-as.vector(prev_proph$trend))^2))/TN)

}

df_rmsse_all_bss <- data.frame(rbind(cbind(id_piezo=code_bss, id_method_ML="ARIMA",type="Local", r=NA, use_exo_rain="False",use_exo_eto="False",use_exo_bdlisa="False",aprox_exo="False" ,rmsse=rmsse_arima_p),cbind(id_piezo=code_bss, id_method_ML="ARIMA",type="Local", r=NA, use_exo_rain="True",use_exo_eto="False",use_exo_bdlisa="False",aprox_exo="False" ,rmsse=rmsse_arima_p_tp),cbind(id_piezo=code_bss, id_method_ML="LM",type="Local", r=k, use_exo_rain="False",use_exo_eto="False",use_exo_bdlisa="False",aprox_exo="False" ,rmsse=rmsse_lm_p),cbind(id_piezo=code_bss, id_method_ML="LM",type="Local", r=k, use_exo_rain="True",use_exo_eto="False",use_exo_bdlisa="False",aprox_exo="False" ,rmsse=rmsse_lm_p_tp),cbind(id_piezo=code_bss, id_method_ML="LM",type="Local", r=k, use_exo_rain="True",use_exo_eto="True",use_exo_bdlisa="False",aprox_exo="False" ,rmsse=rmsse_lm_p_tp_e),cbind(id_piezo=code_bss, id_method_ML="SVR",type="Local", r=k, use_exo_rain="False",use_exo_eto="False",use_exo_bdlisa="False",aprox_exo="False" ,rmsse=rmsse_svr_p),cbind(id_piezo=code_bss, id_method_ML="SVR",type="Local", r=k, use_exo_rain="True",use_exo_eto="False",use_exo_bdlisa="False",aprox_exo="False" ,rmsse=rmsse_svr_p_tp),cbind(id_piezo=code_bss, id_method_ML="SVR",type="Local", r=k, use_exo_rain="True",use_exo_eto="True",use_exo_bdlisa="False",aprox_exo="False" ,rmsse=rmsse_svr_p_tp_e),cbind(id_piezo=code_bss, id_method_ML="PROPHET",type="Local", r=NA, use_exo_rain="False",use_exo_eto="False",use_exo_bdlisa="False",aprox_exo="False" ,rmsse=rmsse_prophet)))
df_rmsse_all_bss$rmsse <- as.numeric(df_rmsse_all_bss$rmsse)



```



