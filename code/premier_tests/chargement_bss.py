#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Chargement des informations sur les stations

Version complete ... mais moche

@author: T. Guyet
@date: 05/2021
"""


import json
import pandas as pd

import requests

def code_dep():
    code_departement=['01','02','03','04','05','06','07','08','09']
    for i in range(10,20):
        code_departement.append(str(i))
    code_departement+=['2A','2B']
    for i in range (21,69):
        code_departement.append(str(i))
    code_departement+=['69D','69M']
    for i in range (70,95):
        code_departement.append(str(i))
    code_departement+=['971','972','973','974','975','976','977','978','984','986','987','988','989']
    return code_departement

depts = code_dep()

stations=[]

next_page = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/stations'
params = {}
while next_page is not None:
    response = requests.get(next_page,params=params)
    print(next_page)
    data=response.json()   
    try:
        next_page = data['next']
    except:
        #il y a eu une erreur / ou c'est la dernière page
        print(data)
        break
    #on récupere les données de la page
    stations += data['data']


#transformation en data.frame pandas
stations=pd.DataFrame(stations)

stations.to_csv('../../data/stations.csv')
