---
title: "Untitled"
output: html_document
---

```{r}
library(tidyverse)
library(tsibble)
library(lubridate)
library(fable)
library(forecast) #requis pour na.interp
```

```{r}
rm(list=ls()) 
data = readRDS('../../data/prev_oct2020_sans_DOM-TOM.csv', ",")

code_bss=data$bss[-which(duplicated(data$bss)) ]


RMSE_arima=rep(NA)
RMSE_ets=rep(NA)
RMSE_mixed=rep(NA)
RMSE_snaive=rep(NA)
    
ME_arima=rep(NA)
ME_ets=rep(NA)
ME_mixed=rep(NA)
ME_snaive=rep(NA)
    
MAE_arima=rep(NA)
MAE_ets=rep(NA)
MAE_mixed=rep(NA)
MAE_snaive=rep(NA)
    
MPE_arima=rep(NA)
MPE_ets=rep(NA)
MPE_mixed=rep(NA)
MPE_snaive=rep(NA)
h=90
for ( i in 1:length(code_bss)) {
  serie1 = data %>% filter(bss==code_bss[i]) %>% select(t,imputed)
  serie1_ts=as_tsibble(serie1,key = NULL,index=t)

  end_tr_date=as.Date(serie1_ts$t[nrow(serie1_ts)-h])
  train <- serie1_ts %>%
    filter(t <= end_tr_date)
  fit <- train %>%
    model(
      ets = ETS(imputed),
      arima = ARIMA(imputed),
      snaive = SNAIVE(imputed)
      ) %>%
    mutate(mixed = (ets + arima + snaive) / 3)
  fc <- fit %>% forecast(h = h)
  fc %>%
    autoplot(serie1_ts, level = NULL) +
    ggtitle("Forecasts ") +
    xlab("Year") +
    guides(colour = guide_legend(title = "Forecast"))
  # on verifie la précision grace à plusieurs mesures d'exctitude ( MAE, MAPE, MASE )

  accuracy(fc, serie1_ts)


  # on examine maintenant la precision grace au CRPS(continuous Rank Probability Scores) et Winkler        Scores (pour des intervalles de prédiction de 95%).

  fc_accuracy <- accuracy(fc, serie1_ts,
    measures = list(
    point_accuracy_measures,
    interval_accuracy_measures,
    distribution_accuracy_measures
    )
  )
  fc_accuracy
  RMSE_arima[i]=fc_accuracy$RMSE[1]
  RMSE_ets[i]=fc_accuracy$RMSE[2]
  RMSE_mixed[i]=fc_accuracy$RMSE[3]
  RMSE_snaive[i]=fc_accuracy$RMSE[4]
    
  ME_arima[i]=fc_accuracy$ME[1]
  ME_ets[i]=fc_accuracy$ME[2]
  ME_mixed[i]=fc_accuracy$ME[3]
  ME_snaive[i]=fc_accuracy$ME[4]
    
  MAE_arima[i]=fc_accuracy$MAE[1]
  MAE_ets[i]=fc_accuracy$MAE[2]
  MAE_mixed[i]=fc_accuracy$MAE[3]
  MAE_snaive[i]=fc_accuracy$MAE[4]
    
  MPE_arima[i]=fc_accuracy$MPE[1]
  MPE_ets[i]=fc_accuracy$MPE[2]
  MPE_mixed[i]=fc_accuracy$MPE[3]
  MPE_snaive[i]=fc_accuracy$MPE[4]
}

#Construction d'un jeu de données près à l'utilisation pour des graphiques
df=data.frame(rbind(cbind(code_bss,RMSE_arima,ME_arima,MAE_arima,MPE_arima,rep("arima")),cbind(code_bss,RMSE_ets,ME_ets,MAE_ets,MPE_ets,rep("ets")),cbind(code_bss,RMSE_mixed,ME_mixed,MAE_mixed,MPE_mixed,rep("mixed")),cbind(code_bss,RMSE_snaive,ME_snaive,MAE_snaive,MPE_snaive,rep("snaive"))))
colnames(df)=c("code_bss","RMSE","ME","MAE","MPE","methode")
df$RMSE=as.numeric(df$RMSE)
df$ME=as.numeric(df$ME)
df$MAE=as.numeric(df$MAE)
df$MPE=as.numeric(df$MPE)
df


View(df)
```



```{r}
p <- ggplot(df, aes(x=methode, y=RMSE, color=methode))
p <- p + geom_boxplot()
p <- p + theme_bw() + ylim(0,1) 
#p <- p + xlab("Date") + ylab("Niveau")
p

p <- ggplot(df, aes(x=methode, y=ME, color=methode))
p <- p + geom_boxplot()
p <- p + theme_bw() + ylim(0,1) 
#p <- p + xlab("Date") + ylab("Niveau")
p


p <- ggplot(df, aes(x=methode, y=MAE, color=methode))
p <- p + geom_boxplot()
p <- p + theme_bw() + ylim(0,1) 
#p <- p + xlab("Date") + ylab("Niveau")
p


p <- ggplot(df, aes(x=methode, y=MPE, color=methode))
p <- p + geom_boxplot()
p <- p + theme_bw() + ylim(0,1) 
#p <- p + xlab("Date") + ylab("Niveau")
p
```