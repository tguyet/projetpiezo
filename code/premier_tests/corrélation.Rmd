---
title: ''
output: html_document
---
```{r}
library(tidyverse)
library(tsibble)
library(forecast)
library(ggplot2)
library(readr)
```


```{r}
#setwd("~/projetpiezo/code/premier_tests")
rm(list=ls()) 
rain_data = read_delim('../../data/rain_bss_2020.csv', ",")
pietzo_data= readRDS('../../data/prev_oct2020_sans_DOM-TOM.csv')
colnames(rain_data)=c("t",'tp','bss')
pietzo_data=pietzo_data[,-3]
colnames(pietzo_data)=c('bss','t','p')

data=merge(rain_data,pietzo_data,key=c('bss','t'))
code_bss=data$bss%>%unique()
```

#fonction de corrélation croisée de l’échantillon (CCF)
# CCF pour une serie 
```{r}
data_bss=data%>%filter(bss==code_bss[1])
#identifie le decalage h de la variable xt+h qui est peut etre prédicteur de yt
ccf_values=ccf(data_bss$tp, data_bss$p)
ccf_values
#donne le décalage h et sa corrélation 

```

Renvoie un data frame contenant tout le lag, et l'acf max de tout les bss
```{r}
data_ccf_max=data.frame()
for (i in 1:length(code_bss)){
  data_bss=data%>%filter(bss==code_bss[i])
  ccf_values=ccf(data_bss$tp, data_bss$p,plot=FALSE)
  data_ccf=data.frame(acf=ccf_values$acf,lag=ccf_values$lag,abs_acf=abs(ccf_values$acf))
  max_acf=which.max(data_ccf$abs_acf)
  ccf_max=data.frame(bss=code_bss[i],data_ccf[max_acf,])
  data_ccf_max=data.frame(rbind(data_ccf_max,ccf_max))
}

```

Répresentation des lag et des corrélations les plus présentes

```{r}
p= ggplot(data_ccf_max, aes(x=lag))+
  geom_bar()+
  theme_bw()+
  xlab("lag")+ylab("effectif")
p

p= ggplot(data_ccf_max, aes(x=acf))+
  geom_histogram()+
  theme_bw()+
  xlab("corrélation")+ylab("effectif")
p
```

on essaye de refaire le ccf avec la dérivé du niveau d'eau 
```{r}
data_ccf_max=data.frame()
for (i in 1:length(code_bss)){
  data_bss=data%>%filter(bss==code_bss[i])
  #calcul de la dérivée
  data_bss[-nrow(data_bss),]$p=(data_bss[-1,]$p-data_bss[-nrow(data_bss),]$p)/2
  data_bss=data_bss[-nrow(data_bss),]
  ccf_values=ccf(data_bss$tp, data_bss$p,plot=FALSE)
  data_ccf=data.frame(acf=ccf_values$acf,lag=ccf_values$lag,abs_acf=abs(ccf_values$acf))
  max_acf=which.max(data_ccf$abs_acf)
  ccf_max=data.frame(bss=code_bss[i],data_ccf[max_acf,])
  data_ccf_max=data.frame(rbind(data_ccf_max,ccf_max))
}
```
```{r}
p= ggplot(data_ccf_max, aes(x=lag))+
  geom_bar()+
  theme_bw()+
  xlab("lag")+ylab("effectif")
p

p= ggplot(data_ccf_max, aes(x=acf))+
  geom_histogram()+
  theme_bw()+
  xlab("corrélation")+ylab("effectif")
p
```


on fait un moving average pour les données 
```{r}
MoveAve <- function(x, width) {
     as.vector( stats::filter(x, rep(1/width, width), sides=2) );
}
ndf1=data.frame()
for (i in 1:length(code_bss)){
  data_bss=data%>%filter(bss==code_bss[i])
  MM=MoveAve(as.vector(data_bss$tp),10)
  ndf1=rbind(ndf1,data.frame(bss=code_bss[i],t=data_bss$t,tp_MM=MM,p=data_bss$p))
}

```


```{r}

ndf1=na.omit(ndf1)
data_ccf_max=data.frame()
for (i in 1:length(code_bss)){
  data_bss=ndf1%>%filter(bss==code_bss[i])
  ccf_values=ccf(data_bss$tp, data_bss$p,plot=FALSE)
  data_ccf=data.frame(acf=ccf_values$acf,lag=ccf_values$lag,abs_acf=abs(ccf_values$acf))
  max_acf=which.max(data_ccf$abs_acf)
  ccf_max=data.frame(bss=code_bss[i],data_ccf[max_acf,])
  data_ccf_max=data.frame(rbind(data_ccf_max,ccf_max))
}


```

```{r}
p= ggplot(data_ccf_max, aes(x=lag))+
  geom_bar()+
  theme_bw()+
  xlab("lag")+ylab("effectif")
p

p= ggplot(data_ccf_max, aes(x=acf))+
  geom_histogram()+
  theme_bw()+
  xlab("corrélation")+ylab("effectif")
p
```
#ccf avec dérivée du niveau sur les moyennes mobile 

```{r}
data_ccf_max=data.frame()
for (i in 1:length(code_bss)){
  data_bss=ndf1%>%filter(bss==code_bss[i])
  #calcul de la dérivée
  data_bss[-nrow(data_bss),]$p=(data_bss[-1,]$p-data_bss[-nrow(data_bss),]$p)/2
  data_bss=data_bss[-nrow(data_bss),]
  ccf_values=ccf(data_bss$tp, data_bss$p,plot=FALSE)
  data_ccf=data.frame(acf=ccf_values$acf,lag=ccf_values$lag,abs_acf=abs(ccf_values$acf))
  max_acf=which.max(data_ccf$abs_acf)
  ccf_max=data.frame(bss=code_bss[i],data_ccf[max_acf,])
  data_ccf_max=data.frame(rbind(data_ccf_max,ccf_max))
}
```
```{r}
p= ggplot(data_ccf_max, aes(x=lag))+
  geom_bar()+
  theme_bw()+
  xlab("lag")+ylab("effectif")
p

p= ggplot(data_ccf_max, aes(x=acf))+
  geom_histogram()+
  theme_bw()+
  xlab("corrélation")+ylab("effectif")
p
```


```{r}
## bss 1
data_bss=ndf1%>%filter(bss==code_bss[1])
plot.new() 
par(mar=c(4,4,3,5)) 
plot(data_bss$p,type='l',col="dark green",axes=F,xlab="",ylab="") 
axis(2,col="dark green",col.axis="dark green",at=seq(0, 100, by=2)) 
mtext("niveau des nappes phréatiques",side=2,line=2.5,col="dark green")  
par(new = T)
plot(data_bss$tp,type='l',col="dark red",axes=F,xlab="",ylab="") 
axis( 4 ,col="dark red",col.axis="dark red",at=seq(0, 0.03, by=0.005)) 
mtext("pluviométrie",side=4,line=2.5,col="dark red")
axis( 1 , ,col="black",col.axis="black",at=seq(0, 400, by=50)) 
mtext("temps en jours",side=1,line=2.5,col="black") 


## bss 2
data_bss=ndf1%>%filter(bss==code_bss[2])
plot.new() 
par(mar=c(4,4,3,5)) 
plot(data_bss$p,type='l',col="dark green",axes=F,xlab="",ylab="") 
axis(2,col="dark green",col.axis="dark green",at=seq(0, 100, by=2)) 
mtext("niveau des nappes phréatiques",side=2,line=2.5,col="dark green")  
par(new = T)
plot(data_bss$tp_MM,type='l',col="dark red",axes=F,xlab="",ylab="") 
axis( 4 ,col="dark red",col.axis="dark red",at=seq(0, 0.03, by=0.005)) 
mtext("pluviométrie",side=4,line=2.5,col="dark red")
axis( 1 , ,col="black",col.axis="black",at=seq(0, 400, by=50)) 
mtext("temps en jours",side=1,line=2.5,col="black") 

## bss 3
data_bss=ndf1%>%filter(bss==code_bss[3])
plot.new() 
par(mar=c(4,4,3,5)) 
plot(data_bss$p,type='l',col="dark green",axes=F,xlab="",ylab="") 
axis(2,col="dark green",col.axis="dark green",at=seq(0, 100, by=2)) 
mtext("niveau des nappes phréatiques",side=2,line=2.5,col="dark green")  
par(new = T)
plot(data_bss$tp_MM,type='l',col="dark red",axes=F,xlab="",ylab="") 
axis( 4 ,col="dark red",col.axis="dark red",at=seq(0, 0.03, by=0.005)) 
mtext("pluviométrie",side=4,line=2.5,col="dark red")
axis( 1 , ,col="black",col.axis="black",at=seq(0, 400, by=50)) 
mtext("temps en jours",side=1,line=2.5,col="black") 

## bss 4
data_bss=ndf1%>%filter(bss==code_bss[4])
plot.new() 
par(mar=c(4,4,3,5)) 
plot(data_bss$p,type='l',col="dark green",axes=F,xlab="",ylab="") 
axis(2,col="dark green",col.axis="dark green",at=seq(0, 100, by=2)) 
mtext("niveau des nappes phréatiques",side=2,line=2.5,col="dark green")  
par(new = T)
plot(data_bss$tp_MM,type='l',col="dark red",axes=F,xlab="",ylab="") 
axis( 4 ,col="dark red",col.axis="dark red",at=seq(0, 0.03, by=0.005)) 
mtext("pluviométrie",side=4,line=2.5,col="dark red")
axis( 1 , ,col="black",col.axis="black",at=seq(0, 400, by=50)) 
mtext("temps en jours",side=1,line=2.5,col="black") 
```




normalized cross correlation 

```{r}

normcorr1=function(lag, data_bss ){ 
  lag_p=data_bss[-(1:(lag+1)),]$p
  lag_tp=data_bss[-((nrow(data_bss)-lag):nrow(data_bss)),]$tp
  norm_corr = sum(lag_tp*lag_p) / sqrt(sum(lag_tp^2)*sum(lag_p^2)) 
  norm_corr
}
# on normalise la fonction de corrélation croisé pour obtenir un coéfficient de corrélation de pearson.
normcorr2=function(lag, data_bss ){ 
  lag_p=data_bss[-(1:(lag+1)),]$p
  lag_tp=data_bss[-((nrow(data_bss)-lag):nrow(data_bss)),]$tp
  norm_corr2=mean((lag_tp-mean(lag_tp))*(lag_p-mean(lag_p)))/(sd(lag_tp)*sd(lag_p))
  norm_corr2
}

normcorr3=function(lag, data_bss ){ 
  lag_p=data_bss[-(1:(lag+1)),]$p
  lag_tp=data_bss[-((nrow(data_bss)-lag):nrow(data_bss)),]$tp
  norm_corr3 = sum((lag_tp-mean(lag_tp))*(lag_p-mean(lag_p))) / sqrt(sum((lag_tp-mean(lag_tp))^2)*sum((lag_p-mean(lag_p))^2)) 
  norm_corr3
}
df_final=data.frame()
for (i in 1:length(code_bss)){
  data_i=data%>%filter(bss==code_bss[i])
  df=data.frame()
  for (lag in 1:100){
    norm_corr1=normcorr1(lag, data_i)
    norm_corr2=normcorr2(lag,data_i)
    norm_corr3=normcorr3(lag, data_i)
    df=rbind(df,data.frame(lag=lag,norm_corr1=norm_corr1,norm_corr2=norm_corr2,norm_corr3=norm_corr3))
  }
  max_norm_corr1=df[which.max(df$norm_corr1),]
  max_norm_corr2=df[which.max(df$norm_corr2),]
  max_norm_corr3=df[which.max(df$norm_corr3),]
  df_final=rbind(df_final,data.frame(bss=code_bss[i],lag1=max_norm_corr1$lag,norm_corr1=max_norm_corr1$norm_corr1,lag2=max_norm_corr2$lag,norm_corr2=max_norm_corr2$norm_corr2,lag3=max_norm_corr3$lag,norm_corr3=max_norm_corr3$norm_corr3))
}
View(df_final)
```

```{r}
p= ggplot(df_final, aes(x=lag1))+
  geom_bar()+
  theme_bw()+
  xlab("lag")+ylab("effectif")
p

p= ggplot(df_final, aes(x=norm_corr1))+
  geom_histogram()+
  theme_bw()+
  xlab("corrélation")+ylab("effectif")
p
```

```{r}
p= ggplot(df_final, aes(x=lag2))+
  geom_bar()+
  theme_bw()+
  xlab("lag")+ylab("effectif")
p

p= ggplot(df_final, aes(x=norm_corr2))+
  geom_histogram()+
  theme_bw()+
  xlab("corrélation")+ylab("effectif")
p
```

```{r}
p= ggplot(df_final, aes(x=lag3))+
  geom_bar()+
  theme_bw()+
  xlab("lag")+ylab("effectif")
p

p= ggplot(df_final, aes(x=norm_corr3))+
  geom_histogram()+
  theme_bw()+
  xlab("corrélation")+ylab("effectif")
p
```