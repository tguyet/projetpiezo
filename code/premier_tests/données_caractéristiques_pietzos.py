# -*- coding: utf-8 -*-
"""
Created on Wed May 19 11:07:44 2021

@author: lbeuchee
"""
import json
import pandas as pd

import requests

def code_dep():
    code_departement=['01','02','03','04','05','06','07','08','09']
    for i in range(10,20):
        code_departement.append(str(i))
    code_departement+=['2A','2B']
    for i in range (21,69):
        code_departement.append(str(i))
    code_departement+=['69D','69M']
    for i in range (70,95):
        code_departement.append(str(i))
    code_departement+=['971','972','973','974','975','976','977','978','984','986','987','988','989']
    return code_departement
#code_departement=code_dep()
lat=[]
long=[]
code_departement=['01','02']
df_final=None
for dpt in code_departement:
    url = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/stations'
    maxsize=100
    params = {'code_departement': dpt, 'format': 'json', 'size': maxsize}
    response = requests.get(url, params=params)
    data=response.json()
    for i in range(data['count']):
        print('\tNbre station:%d'%(data['count']))
        if data['count']>maxsize:
            print("\tWarning: plus de stations que prévu ... les dernières ne seront pas chargées")
    for i in range(min( maxsize, data['count'])):
        if len(data['data']) == 0:
            print("\t %s -> pas de données"%(data['data'][i]['code_bss']))
            continue
        print("\t %s -> ajout %d mesures"%(data['data'][i]['code_bss'], len(data['data'])))
        code_bss=[data['data'][j]['code_bss'] for j in range(data['count']) ]
        code_dep=[data['data'][j]['code_departement'] for j in range(data['count'])]
        geometry=[data['data'][j]['geometry'] for j in range(data['count'])]
        coord=[geometry[k]['coordinates'] for k in range(data['count'])][i]
        lat.append(coord[1])
        long.append(coord[0])
        altitude=[data['data'][j]['altitude_station'] for j in range(data['count']) ]
        df=pd.DataFrame({'code_bss':code_bss, 'code_dep':code_dep,'altitude':altitude})
        if df_final is None:
             df_final=df
        else:
            df_final= pd.concat([df_final,df],ignore_index = True)
df2=pd.DataFrame({'lat':lat,'long':long})
print(df2)
df_final_2=df_final.join(df2)
print(df_final_2)
