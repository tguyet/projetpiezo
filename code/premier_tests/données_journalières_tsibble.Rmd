---
title: "données journalière"
output: html_document
---
```{r}
library(tidyverse)
library(tsibble)
library(lubridate)
library(fable)
library(forecast) #requis pour na.interp
```

```{r}
# Commande de nettoyage de l'ensemble des variables de l'environnement
rm(list=ls()) 
# Chargement des données
data = read_delim('../../data/fichier_01_989.csv', ",")
```

```{r}
code_bss=data$bss[-which(duplicated(data$bss)) ]
class(data)
data=as_tibble(data)
serie1 = data %>% filter(bss==code_bss[12]) %>% select (c(t,p))
serie1$year = as.numeric(format(serie1$t, format = "%Y"))
serie1$month =as.numeric(format(serie1$t, format = "%m"))
serie1$day =as.numeric(format(serie1$t, format = "%d"))


serie1_ts=as_tsibble(serie1,key = NULL,index=t)

serie1_ts_NA=fill_gaps(serie1_ts,.full=FALSE)
serie1_ts_imputed=na.interp(serie1_ts_NA$p)
serie1_ts_NA$imputed=serie1_ts_imputed

head(serie1_ts_NA)
fit <- serie1_ts_NA %>%
  model(
    snaive = SNAIVE(imputed ~ lag("year")),
    ets = ETS(imputed),
    arima = ARIMA(imputed)
  )
fit

fc <- fit %>%
  forecast(h = 90)
fc


```

```{r}
fc %>%
  autoplot(serie1_ts_NA, level = NULL) +
  ggtitle("Forecasts ") +
  xlab("Year") +
  guides(colour = guide_legend(title = "Forecast"))
```
```{r}
#calcule des intervalle de prédiction 

hilo(fc,level = 95)
```


```{r}
#calcule d'exactitude des prévisions

h=90
end_tr_date=as.Date(serie1_ts_NA$t[nrow(serie1_ts_NA)-h])

train <- serie1_ts_NA %>%
  filter(t <= end_tr_date)


fit <- train %>%
  model(
    ets = ETS(imputed),
    arima = ARIMA(imputed),
    snaive = SNAIVE(imputed)
  ) %>%
  mutate(mixed = (ets + arima + snaive) / 3)


fc <- fit %>% forecast(h = h)
fc


fc %>%
  autoplot(serie1_ts_NA, level = NULL) +
  ggtitle("Forecasts ") +
  xlab("Year") +
  guides(colour = guide_legend(title = "Forecast"))


```
```{r}
# on verifie la précision grace à plusieurs mesures d'exctitude ( MAE, MAPE, MASE )

accuracy(fc, serie1_ts_NA)


# on examine maintenant la precision grace au CRPS(continuous Rank Probability Scores) et Winkler Scores (pour des intervalles de prédiction de 95%).

fc_accuracy <- accuracy(fc, serie1_ts_NA,
  measures = list(
    point_accuracy_measures,
    interval_accuracy_measures,
    distribution_accuracy_measures
  )
)
fc_accuracy

rmse_arima=fc_accuracy$RMSE[1]
rmse_arima
View(fc_accuracy)
# Ici le modèle fait mieux sur toutes les mesures d'exactitudes snaive

```

```{r}
data = read_delim('../../data/fichier_01_989.csv', ",")
code_bss=data$bss[-which(duplicated(data$bss)) ]
code_bss=code_bss[1:500]

err_arima=rep(NA)
err_ets=rep(NA)
err_mixed=rep(NA)
err_snaive=rep(NA)
h=90
for ( i in 1:length(code_bss)) {
  serie1 = data %>% filter(bss==code_bss[i]) %>% select(t,p)
  serie1_ts=as_tsibble(serie1,key = NULL,index=t)
  serie1_ts_NA=fill_gaps(serie1_ts,.full=FALSE)
  serie1_ts_imputed=na.interp(serie1_ts_NA$p)
  serie1_ts_NA$imputed=serie1_ts_imputed
  
  
  if (nrow(serie1_ts_NA)>100){
    end_tr_date=as.Date(serie1_ts_NA$t[nrow(serie1_ts_NA)-h])
    train <- serie1_ts_NA %>%
      filter(t <= end_tr_date)
    fit <- train %>%
      model(
        ets = ETS(imputed),
        arima = ARIMA(imputed),
        snaive = SNAIVE(imputed)
        ) %>%
      mutate(mixed = (ets + arima + snaive) / 3)
    fc <- fit %>% forecast(h = h)
    fc %>%
      autoplot(serie1_ts_NA, level = NULL) +
      ggtitle("Forecasts ") +
      xlab("Year") +
      guides(colour = guide_legend(title = "Forecast"))
    # on verifie la précision grace à plusieurs mesures d'exctitude ( MAE, MAPE, MASE )

    accuracy(fc, serie1_ts_NA)


    # on examine maintenant la precision grace au CRPS(continuous Rank Probability Scores) et Winkler        Scores (pour des intervalles de prédiction de 95%).

    fc_accuracy <- accuracy(fc, serie1_ts_NA,
      measures = list(
      point_accuracy_measures,
      interval_accuracy_measures,
      distribution_accuracy_measures
      )
    )
    fc_accuracy
    fc_accuracy_reverse=t(tibble(fc_accuracy$RMSE,fc_accuracy$ME,fc_accuracy$MPE,fc_accuracy$MAE,fc_accuracy$MAPE,fc_accuracy$MASE,fc_accuracy$RMSSE,fc_accuracy$ACF1))
    colnames(fc_accuracy_reverse)=(fc_accuracy$.model)
    fc_accuracy_reverse=as_tibble(cbind(err=c('RMSE','ME','MPE','MAE','MAPE','MASE','RMSSE','ACF1'),fc_accuracy_reverse))
    err_arima[i]=mean(as.numeric(fc_accuracy_reverse$arima))
    err_ets[i]=mean(as.numeric(fc_accuracy_reverse$ets))
    err_mixed[i]=mean(as.numeric(fc_accuracy_reverse$mixed))
    err_snaive[i]=mean(as.numeric(fc_accuracy_reverse$snaive))
    

  } else{
    err_arima[i]=NA
    err_ets[i]=NA
    err_mixed[i]=NA
    err_snaive[i]=NA
  }
}

#Construction d'un jeu de données près à l'utilisation pour des graphiques
df=data.frame(rbind(cbind(code_bss,err_arima,rep("arima")),cbind(code_bss,err_ets,rep("ets")),cbind(code_bss,err_mixed,rep("mixed")),cbind(code_bss,err_snaive,rep("snaive"))))
colnames(df)=c("code_bss","err","methode")
df$err=as.numeric(df$err)
df


```
```{r}



p <- ggplot(df, aes(x=methode, y=err, color=methode))
p <- p + geom_boxplot()
p <- p + theme_bw() + ylim(0,5)
#p <- p + xlab("Date") + ylab("Niveau")
p
```

```{r}
data = read_delim('../../data/fichier_01_989.csv', ",")
code_bss=data$bss[-which(duplicated(data$bss)) ]


RMSE_arima=rep(NA)
RMSE_ets=rep(NA)
RMSE_mixed=rep(NA)
RMSE_snaive=rep(NA)
    
ME_arima=rep(NA)
ME_ets=rep(NA)
ME_mixed=rep(NA)
ME_snaive=rep(NA)
    
MAE_arima=rep(NA)
MAE_ets=rep(NA)
MAE_mixed=rep(NA)
MAE_snaive=rep(NA)
    
MPE_arima=rep(NA)
MPE_ets=rep(NA)
MPE_mixed=rep(NA)
MPE_snaive=rep(NA)
h=90
for ( i in 1:length(code_bss)) {
  serie1 = data %>% filter(bss==code_bss[i]) %>% select(t,p)
  serie1_ts=as_tsibble(serie1,key = NULL,index=t)
  serie1_ts_NA=fill_gaps(serie1_ts,.full=FALSE)
  serie1_ts_imputed=na.interp(serie1_ts_NA$p)
  serie1_ts_NA$imputed=serie1_ts_imputed
  
  
  if (nrow(serie1_ts_NA)>500){
    end_tr_date=as.Date(serie1_ts_NA$t[nrow(serie1_ts_NA)-h])
    train <- serie1_ts_NA %>%
      filter(t <= end_tr_date)
    fit <- train %>%
      model(
        ets = ETS(imputed),
        arima = ARIMA(imputed),
        snaive = SNAIVE(imputed)
        ) %>%
      mutate(mixed = (ets + arima + snaive) / 3)
    fc <- fit %>% forecast(h = h)
    # on verifie la précision grace à plusieurs mesures d'exctitude ( MAE, MAPE, MASE )

    accuracy(fc, serie1_ts_NA)


    # on examine maintenant la precision grace au CRPS(continuous Rank Probability Scores) et Winkler        Scores (pour des intervalles de prédiction de 95%).

    fc_accuracy <- accuracy(fc, serie1_ts_NA,
      measures = list(
      point_accuracy_measures,
      interval_accuracy_measures,
      distribution_accuracy_measures
      )
    )
    fc_accuracy
    RMSE_arima[i]=fc_accuracy$RMSE[1]
    RMSE_ets[i]=fc_accuracy$RMSE[2]
    RMSE_mixed[i]=fc_accuracy$RMSE[3]
    RMSE_snaive[i]=fc_accuracy$RMSE[4]
    
    ME_arima[i]=fc_accuracy$ME[1]
    ME_ets[i]=fc_accuracy$ME[2]
    ME_mixed[i]=fc_accuracy$ME[3]
    ME_snaive[i]=fc_accuracy$ME[4]
    
    MAE_arima[i]=fc_accuracy$MAE[1]
    MAE_ets[i]=fc_accuracy$MAE[2]
    MAE_mixed[i]=fc_accuracy$MAE[3]
    MAE_snaive[i]=fc_accuracy$MAE[4]
    
    MPE_arima[i]=fc_accuracy$MPE[1]
    MPE_ets[i]=fc_accuracy$MPE[2]
    MPE_mixed[i]=fc_accuracy$MPE[3]
    MPE_snaive[i]=fc_accuracy$MPE[4]

  } else{
    RMSE_arima[i]=NA
    RMSE_ets[i]=NA
    RMSE_mixed[i]=NA
    RMSE_snaive[i]=NA
    
    ME_arima[i]=NA
    ME_ets[i]=NA
    ME_mixed[i]=NA
    ME_snaive[i]=NA
    
    MAE_arima[i]=NA
    MAE_ets[i]=NA
    MAE_mixed[i]=NA
    MAE_snaive[i]=NA
    
    MPE_arima[i]=NA
    MPE_ets[i]=NA
    MPE_mixed[i]=NA
    MPE_snaive[i]=NA
  }
}

#Construction d'un jeu de données près à l'utilisation pour des graphiques
df=data.frame(rbind(cbind(code_bss,RMSE_arima,ME_arima,MAE_arima,MPE_arima,rep("arima")),cbind(code_bss,RMSE_ets,ME_ets,MAE_ets,MPE_ets,rep("ets")),cbind(code_bss,RMSE_mixed,ME_mixed,MAE_mixed,MPE_mixed,rep("mixed")),cbind(code_bss,RMSE_snaive,ME_snaive,MAE_snaive,MPE_snaive,rep("snaive"))))
colnames(df)=c("code_bss","RMSE","ME","MAE","MPE","methode")
df$RMSE=as.numeric(df$RMSE)
df$ME=as.numeric(df$ME)
df$MAE=as.numeric(df$MAE)
df$MPE=as.numeric(df$MPE)
df


View(df)
```



```{r}
p <- ggplot(df, aes(x=methode, y=RMSE, color=methode))
p <- p + geom_boxplot()
p <- p + theme_bw() + ylim(0,1) 
#p <- p + xlab("Date") + ylab("Ni)
p

p <- ggplot(df, aes(x=methode, y=ME, color=methode))
p <- p + geom_boxplot()
p <- p + theme_bw() + ylim(0,1) 
#p <- p + xlab("Date") + ylab("Niveau")
p


p <- ggplot(df, aes(x=methode, y=MAE, color=methode))
p <- p + geom_boxplot()
p <- p + theme_bw() + ylim(0,1) 
#p <- p + xlab("Date") + ylab("Niveau")
p


p <- ggplot(df, aes(x=methode, y=MPE, color=methode))
p <- p + geom_boxplot()
p <- p + theme_bw() + ylim(0,1) 
#p <- p + xlab("Date") + ylab("Niveau")
p
```


```{r}
serie1 = data %>% filter(bss==code_bss[1]) %>% select(t,p)
  serie1_ts=as_tsibble(serie1,key = NULL,index=t)
  serie1_ts_NA=fill_gaps(serie1_ts,.full=FALSE)
  serie1_ts_imputed=na.interp(serie1_ts_NA$p)
  serie1_ts_NA$imputed=serie1_ts_imputed
  
  
  if (nrow(serie1_ts_NA)>500){
    end_tr_date=as.Date(serie1_ts_NA$t[nrow(serie1_ts_NA)-h])
    train <- serie1_ts_NA %>%
      filter(t <= end_tr_date)
    fit <- train %>%
      model(
        ets = ETS(imputed),
        arima = ARIMA(imputed),
        snaive = SNAIVE(imputed)
        ) %>%
      mutate(mixed = (ets + arima + snaive) / 3)
    fc <- fit %>% forecast(h = h)
    # on verifie la précision grace à plusieurs mesures d'exctitude ( MAE, MAPE, MASE )

    accuracy(fc, serie1_ts_NA)


    # on examine maintenant la precision grace au CRPS(continuous Rank Probability Scores) et Winkler        Scores (pour des intervalles de prédiction de 95%).

    fc_accuracy <- accuracy(fc, serie1_ts_NA,
      measures = list(
      point_accuracy_measures,
      interval_accuracy_measures,
      distribution_accuracy_measures
      )
    )
    fc_accuracy
  }
```



