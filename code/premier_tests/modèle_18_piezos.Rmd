---
title: "regression_18_piezos"
output: html_document
---
library 
```{r}
library(tidyverse)
library(tsibble)
library(e1071)
library(ggplot2)
library(lubridate)
library(forecast)
```

#recupération des données 
```{r}
# rm(list=ls())
# 
# # fichier des données des données de niveau d'eau de 2015 à 2021
# data = read_delim('fichier_01_989.csv', ",")
# code_bss_data <- data$bss %>% unique()
# 
# #on prend que les 18 piézo qui nous intérressent
# 
# point_eau <- read_delim('../../data/points_eau.csv', ";")
# code_18_bss <- point_eau$CODE_BSS
# data_18_piezo <- data.frame()
# for ( i in 1:length(code_18_bss)) {
#   data_i_piezo <- data %>% filter(bss==code_18_bss[i])
#   data_18_piezo <- rbind(data_18_piezo, data_i_piezo)
# }
# 
# 
# # piezo manquant '01258X0020/S1'  '06505X0080/FORC'  '07223C0113/S"
# 
# #fusion avec les données pluviométriques
# 
# rain_data <-  read_delim('../../data/rain_bss_2015_2020.csv', ",")
# colnames(rain_data) <- c("t",'tp','bss')
# 
# data_p_tp <- merge(rain_data,data_18_piezo,key=c('bss','t'))
# data_p_tp <- data_p_tp[order(data_p_tp[,2]), ]
# 
# 
# # récupération des données d'évapotranspiration
# 
# eto_data <-  read_delim('../../data/eto_bss_2015_2020.csv', ",")
# eto_data <- eto_data %>% select(bss,e,time)
# colnames(eto_data) <- c('bss','e','t')
# 
# data_p_tp_e <- merge(data_p_tp, eto_data, key=c('bss','t'))
# #on recupère les trois séries manquantes
# 
# # données de niveau d'eau
# # serie_07223C0113_S_p <- read_delim('../../data/serie_07223C0113_S.csv', ",")
# # serie_06505X0080_FORC_p<- read_delim('../../data/serie_06505X0080_FORC.csv', ",")
# # serie_01258X0020_S1_p<- read_delim('../../data/serie_01258X0020_S1.csv', ",")
# # serie_manquante_p <-  rbind(serie_07223C0113_S_p , serie_06505X0080_FORC_p , serie_01258X0020_S1_p)
# # serie_manquante_p <- serie_manquante_p[,-3]
# # colnames(serie_manquante_p) <- c('bss',"t", 'p' )
# # serie_manquante_tp <- read_delim('../../data/rain_complement.csv', ",")
# # colnames(serie_manquante_tp) <- c("t", 'tp','bss' )
# # serie_manquante_eto <- read_delim('../../data/eto_complement.csv', ",")
# # serie_manquante_eto <- serie_manquante_eto %>% select(bss,e,time)
# # colnames(serie_manquante_eto) <- c("bss","e", 't' )
# # data_manquante_p_tp <- merge(serie_manquante_p, serie_manquante_tp, key='t')
# # data_manquante <- merge(serie_manquante_p_tp, serie_manquante_eto, key='t')
# # on fusionne ces données avec le reste
# 
# 
# 
# 
# start_sub_serie <- as.Date("2015-01-01",format="%Y-%m-%d")
# end_sub_serie <- as.Date("2020-12-31",format="%Y-%m-%d")
# data <-  data_p_tp_e %>% filter(t>=start_sub_serie &  t<=end_sub_serie)

```

```{r} 
rm(list=ls()) 
data_avec_na <-  read_delim('../../data/dataset_18_2015_2020.csv', ",")
code_bss <-data_avec_na$bss %>% unique() 
# il y a plusieurs données manquantes pour les données de niveau d'eau, on fait donc une imputation de données manquantes
data <- data.frame()
for(i in 1:length(code_bss)){
  bss <- data_avec_na %>% filter(bss==code_bss[i])
  bss_ts <- as_tsibble(bss,key = NULL, index=time)
  bss_ts$imputed <- na.interp(bss_ts$p)
  data <- rbind(data,data.frame(bss=code_bss[i],t=bss_ts$time,e=bss_ts$e,tp=bss_ts$tp,p=bss_ts$imputed))
}


```

fonction
```{r}

fct_matrice <- function(train_df,k){
  vect_p <- train_df[-(1:k),]$p
  vect_tp <- train_df[-(1:k),]$tp
  vect_e <- train_df[-(1:k),]$e
  v <- tibble()
  w <- tibble()
  x <- tibble()
  n <- nrow(train_df)
  
  for (j in 1:(k-1)){
    vect_p_i <-train_df[-c(1:(k-j),(n-j+1):n),]$p
    vect_tp_i <-train_df[-c(1:(k-j),(n-j+1):n),]$tp
    vect_tp_i <-train_df[-c(1:(k-j),(n-j+1):n),]$e
    v <- rbind( vect_p_i, v)
    w <- rbind( vect_tp_i, w)
    x <- rbind( vect_tp_i, x)
  } 
  vect_p_k <- train_df[-((n-k+1):n),]$p 
  vect_tp_k <- train_df[-((n-k+1):n),]$tp
  vect_e_k <- train_df[-((n-k+1):n),]$e
  matrice <- cbind(data.frame(vect_e_k),t(x),data_frame(vect_e),data.frame(vect_tp_k),t(w),data_frame(vect_tp),data.frame(vect_p_k),t(v),data_frame(vect_p))
  colnames(matrice) <- c(paste("e_",seq(k,0,-1), sep=""),paste("tp_",seq(k,0,-1), sep=""),paste("p_",seq(k,0,-1), sep=""))
  rownames(matrice) <- 1:(n-k)
  matrice
} 

#Fonction de Simon pour définir un modèle de regression à partir d'une liste d'indices.
myreg = function(data, idxp, idxc){
  if(length(idxp)==0){
    fff = as.formula(paste(colnames(data)[idxc],"~1", sep = ""))
  } else{
    fff = as.formula(paste(paste(colnames(data)[idxc],"~"),paste(colnames(data)[idxp], collapse = "+")))
  }
  return(lm(fff, data = data))
}

modele_svm = function(data, idxp, idxc){
 
  if(length(idxp)==0){
    fff = as.formula(paste(colnames(data)[idxc],"~1", sep = ""))
  }
  else{
  fff = as.formula(paste(paste(colnames(data)[idxc],"~"),paste(colnames(data)[idxp],
                                                               collapse = "+")))
  }
  return(svm(fff, data = data,type='eps-regression',kernel='radial'))
}


```
on fait le modèle 
```{r}
h <- 93  # taille de l'horizon de prédiction

#on choisit la taille de l'historique
k <- 100  # taille de l'historique

#liste des 18 bss à prévoir
# "00471X0095/PZ2013" "00487X0015/S1"     "00755X0006/S1"    
# "00762X0004/S1"     "00766X0004/S1"     "01258X0020/S1" 
# "01516X0004/S1"     "01584X0023/LV3"    "02206X0022/S1"    
# "02267X0030/S1"     "02603X0009/S1"     "02648X0020/S1"    
# "02706X0074/S77-20" "03124X0088/F"      "04398X0002/SONDAG"
# "06505X0080/FORC"   "07223C0113/S"     "07476X0029/S"

#on choist sur quel bss on travail
bssc="01516X0004/S1"
data_bss <-  data %>% filter(bss ==bssc) %>% select(p,tp,e)
train <-  data_bss[2:(nrow(data_bss) - h),]
train$p_1 <-data_bss[1:(nrow(data_bss) - (h+1)),]$p
train$Tn <- (train$p-train$p_1)^2
TN=sum(train$Tn)/(nrow(data_bss)-1)
df <- fct_matrice(train,k)
lm_p_tp_e <- myreg(df,1:((k*3)+2),(k*3+3))
lm_p_tp <- myreg(df,(k+2):(k*3+2),(k*3+3))
lm_p <- myreg(df,(k*2+3):(k*3+2),(k*3+3))

svr_p_tp_e <- modele_svm(df,1:((k*3)+2),(k*3+3))
svr_p_tp <- modele_svm(df,(k+2):(k*3+2),(k*3+3))
svr_p <- modele_svm(df,(k*2+3):(k*3+2),(k*3+3))


#on cherhce les meilleurs paramètre de svm pour svr_p_tp

formule <- as.formula(paste(paste(colnames(df)[(k*3+3)],"~"),paste(colnames(df)[(k+2):(k*3+2)],
                                                               collapse = "+")))
#tuneResult <- tune(svm, formule,  data = df,
#              ranges = list(epsilon = seq(0,1,0.1), cost = 10^((-2):2))
#)
#plot(tuneResult)
```

préparation du jeu de donnée pour faire les prédictions avec lm et svr 

```{r}
df_test <- tibble()
data_bss_pred <-  data %>% filter(bss == bssc) %>% select(p,tp,e,bss)
if (nrow(data_bss_pred) - h -k <1) {
  print(bss)
  next
}
data_bss_pred <-  data_bss_pred[(nrow(data_bss_pred) - h -k):nrow(data_bss_pred),] 
data_bss_pred$gt <-  data_bss_pred[,1]
data_bss_pred[(nrow(data_bss_pred) - h):nrow(data_bss_pred),1] <- NA
df_test <- rbind(df_test, data_bss_pred)
  
```





prediction avec lm_p
```{r}

df_test_lm_p <- df_test
for (i in (k+1):(93+k+1)){
  data_to_pred_lm_p <- rep(NA)
  for ( w in 1:k){
    data_to_pred_lm_p <- cbind(data_to_pred_lm_p, df_test_lm_p[i-w,1]) 
  }
  data_to_pred_lm_p <- t(data.frame(data_to_pred_lm_p[,-1]))
  colnames(data_to_pred_lm_p) <- paste('p_',1:k,sep='')
  data_to_pred_lm <- data.frame(data_to_pred_lm_p)
  df_test_lm_p[i,1] <-  predict.lm(lm_p,data_to_pred_lm)
}

```

prediction avec lm_p_tp 
```{r}

df_test_lm_p_tp <- df_test
for (i in (k+1):(93+k+1)){
  data_to_pred_lm_tp <- rep(NA)
  data_to_pred_lm_p <- rep(NA)
  for (v in 0:k){
    data_to_pred_lm_tp <- cbind(data_to_pred_lm_tp, df_test_lm_p_tp[i-v,2])
  }
  data_to_pred_lm_tp <- t(data.frame(data_to_pred_lm_tp[,-1]))
  colnames(data_to_pred_lm_tp) <- paste('tp_',0:k,sep='')
  for ( w in 1:k){
    data_to_pred_lm_p <- cbind(data_to_pred_lm_p, df_test_lm_p_tp[i-w,1]) 
  }
  data_to_pred_lm_p <- t(data.frame(data_to_pred_lm_p[,-1]))
  colnames(data_to_pred_lm_p) <- paste('p_',1:k,sep='')
  data_to_pred_lm <- data.frame(data_to_pred_lm_tp,data_to_pred_lm_p)
  df_test_lm_p_tp[i,1] <-  predict.lm(lm_p_tp,data_to_pred_lm)
}

```



prediction avec lm_p_tp_e
```{r}
df_test_lm_p_tp_e <- df_test
for (i in (k+1):(93+k+1)){
  data_to_pred_lm_tp <- rep(NA)
  data_to_pred_lm_e <- rep(NA)
  data_to_pred_lm_p <- rep(NA)
  for (v in 0:k){
    data_to_pred_lm_tp <- cbind(data_to_pred_lm_tp, df_test_lm_p_tp_e[i-v,2])
  }
  data_to_pred_lm_tp <- t(data.frame(data_to_pred_lm_tp[,-1]))
  colnames(data_to_pred_lm_tp) <- paste('tp_',0:k,sep='')
  for (x in 0:k){
    data_to_pred_lm_e <- cbind(data_to_pred_lm_e, df_test_lm_p_tp_e[i-v,3])
  }
  data_to_pred_lm_e <- t(data.frame(data_to_pred_lm_e[,-1]))
  colnames(data_to_pred_lm_e) <- paste('e_',0:k,sep='')
  
  for ( w in 1:k){
    data_to_pred_lm_p <- cbind(data_to_pred_lm_p, df_test_lm_p_tp_e[i-w,1]) 
  }
  data_to_pred_lm_p <- t(data.frame(data_to_pred_lm_p[,-1]))
  colnames(data_to_pred_lm_p) <- paste('p_',1:k,sep='')
  data_to_pred_lm <- data.frame(data_to_pred_lm_e,data_to_pred_lm_tp,data_to_pred_lm_p)
  df_test_lm_p_tp_e[i,1] <-  predict.lm(lm_p_tp_e,data_to_pred_lm)
}



```


RMSE lm
```{r}
rmse_p_tp_e <- sqrt((mean((as.vector(df_test_lm_p_tp_e$p)-as.vector(df_test_lm_p_tp_e$gt))^2))/TN)

rmse_p_tp <- sqrt((mean((as.vector(df_test_lm_p_tp$p)-as.vector(df_test_lm_p_tp$gt))^2))/TN)
#"00471X0095/PZ2013" <- 1.21
rmse_p <- sqrt((mean((as.vector(df_test_lm_p$p)-as.vector(df_test_lm_p$gt))^2))/TN)
#"00471X0095/PZ2013" <- 0.50

```

Visualisation des prédictions avec le modèle lm


```{r}

#lm_p
serie1_pred <-  df_test_lm_p
serie1_pred <- serie1_pred[-(1:(k+1)),]

serie1 <- data %>% filter(bss==bssc) 
s1=rbind(data.frame(niveau=serie1$p,val='valeurs observés',t=seq(as.Date("2015-01-01"),
    as.Date("2020-12-31"), by = "day")),data.frame(niveau=serie1_pred$p,val='valeurs prédites(lm_p)',t=seq(as.Date("2020-09-30"),
+     as.Date("2020-12-31"), by = "day")))
p <- ggplot(data=s1, aes(x=t, y=niveau, color=val))+
  geom_line()+
  xlab('time (day)')+
  ggtitle("00471X0095/PZ2013")+
  theme_bw()
p

#lm_p_tp
serie1_pred= df_test_lm_p_tp
serie1_pred <- serie1_pred[-(1:(k+1)),]

serie1 <- data %>% filter(bss==bssc) 
s1=rbind(data.frame(niveau=serie1$p,val='valeurs observés',t=seq(as.Date("2015-01-01"),
    as.Date("2020-12-31"), by = "day")),data.frame(niveau=serie1_pred$p,val='valeurs prédites(lm_p_tp)',t=seq(as.Date("2020-09-30"),
+     as.Date("2020-12-31"), by = "day")))
p <- ggplot(data=s1, aes(x=t, y=niveau, color=val))+
  geom_line()+
  xlab('time (day)')+
  ggtitle(bssc)+
  theme_bw()
p

#lm_p_tp
serie1_pred= df_test_lm_p_tp_e
serie1_pred <- serie1_pred[-(1:(k+1)),]

serie1 <- data %>% filter(bss==bssc) 
s1=rbind(data.frame(niveau=serie1$p,val='valeurs observés',t=seq(as.Date("2015-01-01"),
    as.Date("2020-12-31"), by = "day")),data.frame(niveau=serie1_pred$p,val='valeurs prédites(lm_p_tp_e)',t=seq(as.Date("2020-09-30"),
+     as.Date("2020-12-31"), by = "day")))
p <- ggplot(data=s1, aes(x=t, y=niveau, color=val))+
  geom_line()+
  xlab('time (day)')+
  ggtitle(bssc)+
  theme_bw()
p
```
modèle svr 


prediction avec svr_p
```{r}

  # On construit une ligne de prédiction à faire par piezo (une ligne toutes les 98 pour le jeu de données)
df_test_svr_p <- df_test
for (i in (k+1):(93+k+1)){
  data_to_pred_svr_p <- rep(NA)
  for ( w in 1:k){
    data_to_pred_svr_p <- cbind(data_to_pred_svr_p, df_test_svr_p[i-w,1]) 
  }
  data_to_pred_svr_p <- t(data.frame(data_to_pred_svr_p[,-1]))
  colnames(data_to_pred_svr_p) <- paste('p_',1:k,sep='')
  data_to_pred_svr <- data.frame(data_to_pred_svr_p)
  df_test_svr_p[i,1] <-  predict(svr_p,data_to_pred_svr)
}

```

prediction avec svr_p_tp 
```{r}

df_test_svr_p_tp <- df_test
for (i in (k+1):(93+k+1)){
  data_to_pred_svr_tp <- rep(NA)
  data_to_pred_svr_p <- rep(NA)
  for (v in 0:k){
    data_to_pred_svr_tp <- cbind(data_to_pred_svr_tp, df_test_svr_p_tp[i-v,2])
  }
  data_to_pred_svr_tp <- t(data.frame(data_to_pred_svr_tp[,-1]))
  colnames(data_to_pred_svr_tp) <- paste('tp_',0:k,sep='')
  for ( w in 1:k){
    data_to_pred_svr_p <- cbind(data_to_pred_svr_p, df_test_svr_p_tp[i-w,1]) 
  }
  data_to_pred_svr_p <- t(data.frame(data_to_pred_svr_p[,-1]))
  colnames(data_to_pred_svr_p) <- paste('p_',1:k,sep='')
  data_to_pred_svr <- data.frame(data_to_pred_svr_tp,data_to_pred_svr_p)
  df_test_svr_p_tp[i,1] <-  predict(svr_p_tp,data_to_pred_svr)
}

```



prediction avec svr_p_tp_e
```{r}
df_test_svr_p_tp_e <- df_test
for (i in (k+1):(93+k+1)){
  data_to_pred_svr_tp <- rep(NA)
  data_to_pred_svr_e <- rep(NA)
  data_to_pred_svr_p <- rep(NA)
  for (v in 0:k){
    data_to_pred_svr_tp <- cbind(data_to_pred_svr_tp, df_test_svr_p_tp_e[i-v,2])
  }
  data_to_pred_svr_tp <- t(data.frame(data_to_pred_svr_tp[,-1]))
  colnames(data_to_pred_svr_tp) <- paste('tp_',0:k,sep='')
  for (x in 0:k){
    data_to_pred_svr_e <- cbind(data_to_pred_svr_e, df_test_svr_p_tp_e[i-v,3])
  }
  data_to_pred_svr_e <- t(data.frame(data_to_pred_svr_e[,-1]))
  colnames(data_to_pred_svr_e) <- paste('e_',0:k,sep='')
  
  for ( w in 1:k){
    data_to_pred_svr_p <- cbind(data_to_pred_svr_p, df_test_svr_p_tp_e[i-w,1]) 
  }
  data_to_pred_svr_p <- t(data.frame(data_to_pred_svr_p[,-1]))
  colnames(data_to_pred_svr_p) <- paste('p_',1:k,sep='')
  data_to_pred_svr <- data.frame(data_to_pred_svr_e,data_to_pred_svr_tp,data_to_pred_svr_p)
  df_test_svr_p_tp_e[i,1] <-  predict(svr_p_tp_e,data_to_pred_svr)
}



```
RMSE svr
```{r}
rmse_svr_p_tp_e= sqrt(mean((as.vector(df_test_svr_p_tp_e$p)-as.vector(df_test_svr_p_tp_e$gt))^2))
rmse_svr_p_tp_e
rmse_svr_p_tp= sqrt(mean((as.vector(df_test_svr_p_tp$p)-as.vector(df_test_svr_p_tp$gt))^2))
#"00471X0095/PZ2013" <- 1.21
rmse_svr_p= sqrt(mean((as.vector(df_test_svr_p$p)-as.vector(df_test_svr_p$gt))^2))
#"00471X0095/PZ2013" <- 0.50

```


Visualisation des prédictions avec le modèle svr

```{r}

#lm_p
serie1_pred <-  df_test_svr_p
serie1_pred <- serie1_pred[-(1:(k+1)),]

serie1 <- data %>% filter(bss==bssc) 
s1=rbind(data.frame(niveau=serie1$p,val='valeurs observés',t=seq(as.Date("2015-01-01"),
    as.Date("2020-12-31"), by = "day")),data.frame(niveau=serie1_pred$p,val='valeurs prédites(svr_p)',t=seq(as.Date("2020-09-30"),
+     as.Date("2020-12-31"), by = "day")))
p <- ggplot(data=s1, aes(x=t, y=niveau, color=val))+
  geom_line()+
  xlab('time (day)')+
  ggtitle("00471X0095/PZ2013")+
  theme_bw()
p

#lm_p_tp
serie1_pred= df_test_svr_p_tp
serie1_pred <- serie1_pred[-(1:(k+1)),]

serie1 <- data %>% filter(bss==bssc) 
s1=rbind(data.frame(niveau=serie1$p,val='valeurs observés',t=seq(as.Date("2015-01-01"),
    as.Date("2020-12-31"), by = "day")),data.frame(niveau=serie1_pred$p,val='valeurs prédites(svr_p_tp)',t=seq(as.Date("2020-09-30"),
+     as.Date("2020-12-31"), by = "day")))
p <- ggplot(data=s1, aes(x=t, y=niveau, color=val))+
  geom_line()+
  xlab('time (day)')+
  ggtitle(bssc)+
  theme_bw()
p


#lm_p_tp
serie1_pred= df_test_svr_p_tp_e
serie1_pred <- serie1_pred[-(1:(k+1)),]

serie1 <- data %>% filter(bss==bssc) 
s1=rbind(data.frame(niveau=serie1$p,val='valeurs observés',t=seq(as.Date("2015-01-01"),
    as.Date("2020-12-31"), by = "day")),data.frame(niveau=serie1_pred$p,val='valeurs prédites(lm_p_tp_e)',t=seq(as.Date("2020-09-30"),
+     as.Date("2020-12-31"), by = "day")))
p <- ggplot(data=s1, aes(x=t, y=niveau, color=val))+
  geom_line()+
  xlab('time (day)')+
  ggtitle(bssc)+
  theme_bw()
p
```



prediction avec arima en tenant compte que du niveau d'eau, en regroupant les données par mois 

```{r}
h=93
data_bss = data %>% filter(bss==bssc) %>% select(t,tp,p)
train <-  data_bss[2:(nrow(data_bss) - h),]
train$p_1 <-data_bss[1:(nrow(data_bss) - (h+1)),]$p
train$Tn <- (train$p-train$p_1)^2
test <- data_bss[nrow(data_bss-h+1):(nrow(data_bss)),]
TN=sum(train$Tn)/(nrow(data_bss)-1)

# prediction arima avec p 
fit_p <- auto.arima(train$p)
fcast_p <- forecast(fit_p, h =h)
all_fcast_p <- data.frame(prev=as.vector(fcast_p[["mean"]]))
#prediction ariam avec p et tp 
pluv_vect_prev <- as.vector(test$tp)
fit_p_tp <- auto.arima(train$p, xreg = train$tp)
fcast_tp <- pluv_vect_prev
fcast_p_tp <- forecast(fit_p_tp, xreg = fcast_tp, h = h)
all_fcast_p_tp <- data.frame(prev=as.vector(fcast_p_tp[["mean"]]))

rmsse_arima_p <- sqrt(mean((as.vector(test$p)-as.vector(all_fcast_p$prev))^2))
#0.5983971
rmsse_arima_p_tp <- sqrt(mean((as.vector(test$p)-as.vector(all_fcast_p_tp$prev))^2))
#0.2571306
```

graphique des previsions avec ARIMA 

```{r}


#lm_p
serie1_pred <- all_fcast_p
serie1 <- ss
s1=rbind(data.frame(niveau=serie1$p,val='valeurs observés',t=seq(as.Date("2015-01-01"),
    as.Date("2020-12-01"), by = "month")),data.frame(niveau=serie1_pred$prev,val='valeurs prédites(arima_p)',t=seq(as.Date("2020-10-01"),
+     as.Date("2020-12-01"), by = "month")))
p <- ggplot(data=s1, aes(x=t, y=niveau, color=val))+
  geom_line()+
  xlab('time (day)')+
  ggtitle("00471X0095/PZ2013")+
  theme_bw()
p


#lm_p_tp
serie1_pred <- all_fcast_p_tp

serie1 <- ss 
s1=rbind(data.frame(niveau=serie1$p,val='valeurs observés',t=seq(as.Date("2015-01-01"),
    as.Date("2020-12-01"), by = "month")),data.frame(niveau=serie1_pred$prev,val='valeurs prédites(arima_p_tp)',t=seq(as.Date("2020-10-01"),
+     as.Date("2020-12-01"), by = "month")))
p <- ggplot(data=s1, aes(x=t, y=niveau, color=val))+
  geom_line()+
  xlab('time (day)')+
  ggtitle("00471X0095/PZ2013")+
  theme_bw()
p
```


prediction avec prophet


```{r}

h=93
bssc="01516X0004/S1"
data_bss <-  data %>% filter(bss ==bssc) %>% select(t,p)
train <-  data_bss[1:(nrow(data_bss) - h),]
test <- data_bss[(nrow(data_bss) - h+1):(nrow(data_bss)),]
names(train) <- c('ds', 'y') 
m <- prophet(train)
future <- make_future_dataframe(m, periods=h)
proph <- predict(m, future)
prev_proph <- proph[(nrow(proph) - h+1):(nrow(proph)),]
rmse_prophet <- sqrt(mean((as.vector(test$p)-as.vector(prev_proph$trend))^2))

```

