#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generation des séries journalières de pluviométrie en 2020 

- pré-requis
  * génération de rain_2020.nc à partir du script 'load_cdc.py'
  * generation de la liste de bss d'intérêts à l'aide de 'liste_bss_sous-serie2020.Rmd''

@author: T. Guyet, Institut Agro/IRISA
"""

import pandas as pd
import numpy as np
import xarray as xr
import os
import datetime
from tqdm import tqdm
import requests

list_bss=["00471X0095/PZ2013","00487X0015/S1","00755X0006/S1"  ,"00762X0004/S1","00766X0004/S1","01258X0020/S1","01516X0004/S1","01584X0023/LV3","02206X0022/S1","02267X0030/S1","02603X0009/S1","02648X0020/S1","02706X0074/S77-20","03124X0088/F","04398X0002/SONDAG","06505X0080/FORC","07223C0113/S","07476X0029/S"]

stations=[]
for code_bss in list_bss:
    req = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/stations'
    params = {"code_bss":code_bss, "format":"json", 'size': 20}
    response = requests.get(req,params=params)
    data=response.json() 
    #on récupere les données de la page
    stations += data['data']
stations=pd.DataFrame(stations)
#ouverture du fichier (existant ou chargé à l'instant)


#######################################################"
# On commence par le niveau des eaux pour les piezos

levels=[]
for bss in list_bss:
    url = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/chroniques'
    params = {'code_bss':bss, 'date_debut_mesure':'2015-01-01', 'size':5000}
    response = requests.get(url, params=params)
    val=response.json()
    dates = [ val['data'][j]["date_mesure"] for j in range(len(val['data']) )]
    vals = [ val['data'][i]["profondeur_nappe"] for i in range(len(val['data']) )]
    bss=[ bss ]*len(vals)
    levels.append( pd.DataFrame({'t':pd.to_datetime(dates), 'p':vals, 'bss':bss}) )

levels = pd.concat(levels)
levels=levels.rename(columns={"t":"time"}).set_index(['bss','time'])


#######################################################"
# On enchaine par la pluie

rain=[]
for year in [2020,2019,2018,2017,2016,2015]:
    #Ouverture du fichier contenant les données de pluie
    ds_rain = xr.open_dataset("../../data/rain_"+str(year)+".nc")
    df = ds_rain.to_dataframe()
    df.reset_index(inplace=True)
    
    s = pd.merge(pd.DataFrame({'bss':list_bss}), stations, left_on="bss", right_on="code_bss", how="left")
    s = s[['code_bss','x', 'y', 'geometry', 'code_departement','profondeur_investigation', 'altitude_station', 'noms_masse_eau_edl']]
    s.columns = ['bss','x', 'y', 'geometry', 'dpt','prof', 'alt', 'masse_eau']
    for v in s.iterrows():
        x=v[1]['x']
        y=v[1]['y']
        try:
            l=df[(df.longitude>=x) & (df.longitude<x+0.25) & (df.latitude>=y) & (df.latitude<y+0.25)][['time','tp']].resample('D', on='time').sum()
            l['bss']=v[1]['bss']
            rain.append(l)
        except AttributeError:
            print("error: possible unknown location (%f,%f)"%(x,y))
        
rain=pd.concat(rain)
rain=rain.reset_index().set_index(['bss','time'])



#####################################################
# On continue avec l'evapotranspiration


eto=[]
for year in [2020,2019,2018,2017,2016,2015]:
    print(f"year: {year}")
    print("load dataframe")
    ds_eto = xr.open_dataset("../../data/total_evaporation_"+str(year)+".nc")
    #aggregate hourly data by day of year
    ds_eto_agg=ds_eto.groupby("time.dayofyear").sum()
    
    print("iteration to do: ", len(s))
    for v in tqdm(s.iterrows()):
        x=v[1]['x']
        y=v[1]['y']
        try:
            l=ds_eto_agg.where( (ds_eto_agg.longitude>=x) & 
                 (ds_eto_agg.longitude<x+0.10) & 
                 (ds_eto_agg.latitude>=y) & 
                 (ds_eto_agg.latitude<y+0.10) , drop=True).to_dataframe()
            l['bss']=v[1]['bss']
            l.reset_index(inplace=True)
            l['time']=(np.asarray(year, dtype='datetime64[Y]')-1970) +(np.asarray(l['dayofyear'], dtype='timedelta64[D]')-1)
            eto.append(l)
        except AttributeError:
            print("error: possible unknown location (%f,%f)"%(x,y))
   
alleto=pd.concat(eto)
#alleto.sort_values(['bss','time'], inplace=True)
alleto=alleto.reset_index().set_index(['bss','time'])[['e']]

dataset=rain.join(alleto).join(levels)
dataset.to_csv("../../data/dataset_18_2015_2020.csv")
