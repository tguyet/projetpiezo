#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import xarray as xr
import os
import datetime
from tqdm import tqdm
import requests

#Chargement de la liste des BSS d'intérêt
bss=pd.read_csv("../../data/nouvelle_liste_bss_avec_DOM-TOM.csv")

stations_file="../../data/stations_bss_2020_v2.csv"
if not os.path.exists(stations_file):
    # À calculer une fois ...
    stations=[]
    for code_bss in bss.x:
        req = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/stations'
        params = {"code_bss":code_bss, "format":"json", 'size': 20}
        response = requests.get(req,params=params)
        data=response.json() 
        #on récupere les données de la page
        stations += data['data']
    stations=pd.DataFrame(stations)
    stations.to_csv(stations_file)
#ouverture du fichier (existant ou chargé à l'instant)
stations = pd.read_csv(stations_file)

s = pd.merge(bss, stations, left_on="bss", right_on="code_bss", how="left")
s = s[['code_bss','x', 'y', 'geometry', 'code_departement','profondeur_investigation', 'altitude_station', 'noms_masse_eau_edl']]
s.columns = ['bss','x', 'y', 'geometry', 'dpt','prof', 'alt', 'masse_eau']

eto=[]
for year in [2020,2019,2018,2017,2016,2015]:
    print(f"year: {year}")
    print("load dataframe")
    ds_eto = xr.open_dataset("../../data/total_evaporation_"+str(year)+".nc")
    #aggregate hourly data by day of year
    ds_eto_agg=ds_eto.groupby("time.dayofyear").sum()
    
    print("iteration to do: ", len(s))
    for v in tqdm(s.iterrows()):
        x=v[1]['x']
        y=v[1]['y']
        try:
            l=ds_eto_agg.where( (ds_eto_agg.longitude>=x) & 
                 (ds_eto_agg.longitude<x+0.10) & 
                 (ds_eto_agg.latitude>=y) & 
                 (ds_eto_agg.latitude<y+0.10) , drop=True).to_dataframe()
            l['bss']=v[1]['bss']
            l.reset_index(inplace=True)
            l['time']=(np.asarray(year, dtype='datetime64[Y]')-1970) +(np.asarray(l['dayofyear'], dtype='timedelta64[D]')-1)
            eto.append(l)
        except AttributeError:
            print("error: possible unknown location (%f,%f)"%(x,y))
   
alleto=pd.concat(eto)
alleto.sort_values(['bss','time'], inplace=True)
alleto.to_csv("../../data/eto_bss_2015_2020.csv")
