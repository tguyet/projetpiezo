#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generation des séries journalières de pluviométrie en 2020 

- pré-requis
  * génération de rain_2020.nc à partir du script 'load_cdc.py'
  * generation de la liste de bss d'intérêts à l'aide de 'liste_bss_sous-serie2020.Rmd''

@author: T. Guyet, Institut Agro/IRISA
"""

import pandas as pd
import numpy as np
import xarray as xr
import os
import datetime
import requests

#Chargement de la liste des BSS d'intérêt
bss=pd.read_csv("../../data/liste_bss_2020.csv")

stations_file="../../data/stations_bss_2020.csv"
if not os.path.exists(stations_file):
    # À calculer une fois ...
    stations=[]
    for code_bss in bss.x:
        req = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/stations'
        params = {"code_bss":code_bss, "format":"json", 'size': 20}
        response = requests.get(req,params=params)
        data=response.json() 
        #on récupere les données de la page
        stations += data['data']
    stations=pd.DataFrame(stations)
    stations.to_csv(stations_file)
#ouverture du fichier (existant ou chargé à l'instant)
stations = pd.read_csv(stations_file)


#######################################################"
# On commence par la pluie

#Ouverture du fichier contenant les données de pluie
ds_rain = xr.open_dataset("../../data/rain_2020.nc")
df = ds_rain.to_dataframe()
df.reset_index(inplace=True)

s = pd.merge(bss, stations, left_on="x", right_on="code_bss", how="left")
s = s[['code_bss','x_y', 'y', 'geometry', 'code_departement','profondeur_investigation', 'altitude_station', 'noms_masse_eau_edl']]
s.columns = ['bss','x', 'y', 'geometry', 'dpt','prof', 'alt', 'masse_eau']

rain=[]
for v in s.iterrows():
    x=v[1]['x']
    y=v[1]['y']
    try:
        l=df[(df.longitude>=x) & (df.longitude<x+0.25) & (df.latitude>=y) & (df.latitude<y+0.25)][['time','tp']].resample('D', on='time').sum()
        l['bss']=v[1]['bss']
        rain.append(l)
    except AttributeError:
        print("error: possible unknown location (%f,%f)"%(x,y))
    
rain=pd.concat(rain)
rain.to_csv("../../data/rain_bss_2020.csv")


#####################################################
# On continue avec l'evapotranspiration


ds_eto = xr.open_dataset("../../data/eto_month.nc")
df = ds_eto.to_dataframe()
df.reset_index(inplace=True)
df['time'] = pd.to_datetime( df['time'] )
#df.set_index(['latitude', 'longitude', 'time'],inplace=True)

debut=datetime.datetime(2020,1,1)
fin=datetime.datetime(2021,1,31)
df=df[(df['time'] < fin) & (df['time'] > debut)]
## VIDE .... données que jusqu'à 2019

"""

s = pd.merge(bss, stations, left_on="x", right_on="code_bss", how="left")
s = s[['code_bss','x_y', 'y', 'geometry', 'code_departement','profondeur_investigation', 'altitude_station', 'noms_masse_eau_edl']]
s.columns = ['bss','x', 'y', 'geometry', 'dpt','prof', 'alt', 'masse_eau']

eto=[]
for v in s.iterrows():
    x=v[1]['x']
    y=v[1]['y']
    try:
        l=df[(df.longitude>=x) & (df.longitude<x+0.25) & (df.latitude>=y) & (df.latitude<y+0.25)][['time','tp']].resample('D', on='time').sum()
        l['bss']=v[1]['bss']
        rain.append(l)
    except AttributeError:
        print("error: possible unknown location (%f,%f)"%(x,y))
    
rain=pd.concat(rain)
rain.to_csv("../../data/rain_bss_2020.csv")
"""
