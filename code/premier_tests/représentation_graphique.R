#chargement de mes librairies favorites

library(readr)
library(ggplot2)
library(dplyr)
library(tidyr)
library(naniar)

# ressource qui me semble utile à regarder 
#   -> https://juba.github.io/tidyverse/06-tidyverse.html
#   -> https://juba.github.io/tidyverse/08-ggplot2.html
#   -> https://thinkr.fr/pdf/ggplot2-french-cheatsheet.pdf

#chargement du jeu de données (détecte les date notamment)
# -> le format de data est un "tibble": c'est comme un data.frame, mais en plus riche !
data = read_delim('../data/fichier_01_989.csv', ",")



code_bss=data$bss[-which(duplicated(data$bss)) ]


# selection des données selon un critère, puis selection de deux colonnes

#     -> l'utilisation des "%>" permet d'enchainer des traitements 

#     -> tu peux commencer par faire des versions intermédiaires des données !

serie1 = data %>% filter(bss==code_bss[1]) %>% select(t,p)
serie1



# affichage d'un graphique à l'aide de ggplot

# https://www.r-graph-gallery.com/279-plotting-time-series-with-ggplot2.html

p <- ggplot(serie1, aes(x=t, y=p)) # on utilise le dataframe serie, et le aes décrit comment associer des colonnes du tableau à des caractéristiques graphiques d'une visualisation

p <- p + geom_line() # on veux dessiner des lignes
p <- p + theme_bw() # le theme permet de mettre de la mise en forme (couleur de fond, etc)
p <- p + xlab("Date") + ylab("Niveau") #exemple de label
p

# Autre afficahge ... un boxplot (pour illustrer que c'est quasi-pareil ...)

#  boxplot = boite à moustaches

p <- ggplot(serie1, aes(y=p))
p <- p + geom_boxplot()
p <- p + theme_bw()
p <- p + xlab("Date") + ylab("Niveau")
p

##########################
# Maintenant ... on fait un peu plus compliqué !

# je veux afficher les différentes années sur le même graphique !

serie1$year = format(serie1$t, format = "%Y")
serie1$day = as.numeric(format(serie1$t, format = "%j"))
serie1$month =as.numeric(format(serie1$t, format = "%m"))
p <- ggplot(serie1, aes(x=day, y=p, color=year))
p <- p + geom_line()
p <- p + theme_bw()
p <- p + xlab("Date") + ylab("Niveau")
p


p <- ggplot(serie1, aes(x=day, y=p))
p <- p + geom_smooth()
p <- p + theme_bw()
p <- p + xlab("Date") + ylab("Niveau")
p


#là, je suis surpris du résultat :/

#################################

# library(fda)

# -> à regarder éventuellement

# une autre librairie très spécifique qui permet de visualiser des "functional boxplots"

### j'ai reprit ce que vous aviez fait en mettant les deux sur le meme graphique
p <- ggplot(serie1, aes(x=day, y=p, color=year)) +
  geom_line()+
  geom_smooth(col='red')+
  theme_bw() +
  xlab("Date") + ylab("Niveau")
p


### boxplot de la profondeur pour chaque années

p <- ggplot(serie1, aes(x=year, y=p, varwidth = TRUE))+
  geom_boxplot()+
  theme_bw()+
  xlab("Date") + ylab("Niveau")
p
p <- ggplot(serie1, aes(x=year, y=p, varwidth = TRUE))+
  geom_boxplot()+
  geom_jitter(color = "red", alpha = 0.2)+
  theme_bw()+
  xlab("Date") + ylab("Niveau")
p


###représentation de l'évolution de la profondeur des pietzo par années

mean_year <- aggregate(p~year, data= serie1, FUN=mean)
mean_year
p <- ggplot(mean_year, aes(x=year,y=p))
p <- p + geom_point()
p <- p + theme_bw()
p <- p + xlab("Date") + ylab("Niveau")
p

mean_month=aggregate(serie1$p,list(serie1$month,serie1$year), mean)
mean_month
colnames(mean_month)= c('month','year','p')
p <- ggplot(mean_month, aes(x=month, y=p, color=year)) +
  geom_line()+ 
  theme_bw()+
  xlab("Mois") + ylab("Niveau")
p


### repartition de la profondeur
p= ggplot(serie1) + 
  geom_histogram(aes(x = p)) +
  theme_bw()+
  labs(x = "profondeur", y = "effectif") 
p


### densité de la profondeur

p= ggplot(serie1) + 
  geom_density(aes(x = p)) +
  theme_bw()+
  labs(x = "Date", y = "Niveau") 
p

### fonction de répartion cumulée

p= ggplot(serie1) + 
  stat_ecdf(aes(x = p)) +
  theme_bw()+
  labs(x = "Date", y = "Niveau") 
p


##nuage de point 

p= ggplot(serie1) + 
  geom_point(aes(x = t, y=p), alpha=0.2) +
  theme_bw()+
  labs(x = "Date", y = "Niveau") 
p

library(lubridate)


serie1$yearmonth=ymd(paste0(serie1$year,'-',serie1$month,'-01'))
serie1 %>%
  group_by(year, yearmonth) %>%
  dplyr::summarise(p = mean(p, na.rm = TRUE)) %>%
  ggplot(aes(x=yearmonth, y=p)) +
  facet_wrap(~year, scales = "free_x", ncol = 4) +
  geom_line()+
  theme_bw()+
  labs(x = "Date", y = "Niveau") 


####serie temporelle##

