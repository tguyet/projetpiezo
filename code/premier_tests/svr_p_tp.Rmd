---
title: "svm_p_tp"
output: html_document
---

```{r}
library(tidyverse)
library(tsibble)
library(e1071)
library(ggplot2)
```
##data_480_ piezomètres
```{r}
rm(list=ls())
bdlisa <- readRDS("../../data/EH_bss.RDS")
bdlisa <- bdlisa[-which(duplicated(bdlisa$bss)),]
rain_data = read_delim('../../data/rain_bss_2020.csv', ",")
pietzo_data= readRDS('../../data/prev_oct2020_sans_DOM-TOM.csv')
colnames(rain_data)=c("t",'tp','bss')
pietzo_data=pietzo_data[,-3]
colnames(pietzo_data)=c('bss','t','p')

data=merge(rain_data,pietzo_data,key=c('bss','t'))
data=data[order(data[,2]), ]
code_bss=data$bss%>%unique()
```

##data_2097_piezomètres
```{r}
# rm(list=ls())
# bdlisa <- readRDS("../../data/EH_bss.RDS")
# bdlisa <- bdlisa[-which(duplicated(bdlisa$bss)),]
# rain_data = read_delim('../../data/rain_bss_2020_v2.csv', ",")
# piezo_data= readRDS('../../data/data_piezo_v2.csv')
# colnames(rain_data)=c("t",'tp','bss')
# piezo_data=piezo_data[,-3]
# colnames(piezo_data)=c('bss','t','p')
# 
# 
# data <- merge(rain_data,piezo_data,key=c('bss','t'))
# data=data[order(data[,2]), ]
# code_bss<- data$bss %>% unique()
```

Création de la matrice 
```{r}
fct_matrice <- function(train_df,k){
  vect_p <- train_df[-(1:k),]$p
  vect_tp <- train_df[-(1:k),]$tp
  v <- data.frame()
  w <- data.frame()
  n <- nrow(train_df)
  
  for (j in 1:(k-1)){
    vect_p_i <-train_df[-c(1:(k-j),(n-j+1):n),]$p
    vect_tp_i <-train_df[-c(1:(k-j),(n-j+1):n),]$tp
    v <- rbind( vect_p_i,v)
    w<- rbind( vect_tp_i,w)
  } 
  colnames(v) <- 1:(n-k)
  colnames(w) <- 1:(n-k)
  vect_p_k <- train_df[-((n-k+1):n),]$p 
  vect_tp_k <- train_df[-((n-k+1):n),]$tp
  matrice <- cbind(data.frame(vect_tp_k),t(w),data_frame(vect_tp),data.frame(vect_p_k),t(v),data_frame(vect_p))
  
} 
k=10
h=93
df <- data.frame()
for ( i in 1:length(code_bss)){
  bss1 <-  data %>% filter(bss == code_bss[i]) %>% select(p,tp)
  train <-  bss1[1:(nrow(bss1) - h),]
  df <- rbind(df, data.frame(fct_matrice(train,k),bss=code_bss[i]))
}
colnames(df)=c(sort(c(paste('tp_t_',0:k,sep=''),paste('p_t_',0:k,sep='')),decreasing=TRUE),'bss')


```

#On fusionne matrice p tp code_bss et bdlisa
```{r}
matrice <- merge(bdlisa,df,key='bss')
```

on fait le modèle 
```{r}
col_p_tp <- 9:((k*2)+9)
col_p <- (8+k+2):((k*2)+9)
col_p_tp_bdlisa <- 4:((k*2)+9)
  
  
modele_svm = function(data, idxp, idxc){
 
  if(length(idxp)==0){
    fff = as.formula(paste(colnames(data)[idxc],"~1", sep = ""))
  }
  else{
  fff = as.formula(paste(paste(colnames(data)[idxc],"~"),paste(colnames(data)[idxp],
                                                               collapse = "+")))
  }
  return(svm(fff, data = data,type='eps-regression',kernel='radial'))
}

modele_p <- modele_svm(matrice,col_p,(k*2)+10)
modele_p_tp <- modele_svm(matrice,col_p_tp,(k*2)+10)
modele_p_tp_bdlisa <- modele_svm(matrice,col_p_tp_bdlisa,(k*2)+10)
```

on effecue les previsions 


```{r}
df_test <- tibble()
for ( bssc in code_bss) {
  data_bss_pred = data %>% filter(bss == bssc) %>% select(p,tp,bss)
  if (nrow(data_bss_pred) - h -k <1) {
    print(bss)
    next
  }
  data_bss_pred = data_bss_pred[(nrow(data_bss_pred) - h -k):nrow(data_bss_pred),] 
  data_bss_pred$gt = data_bss_pred[,1]
  data_bss_pred[(nrow(data_bss_pred) - h):nrow(data_bss_pred),1] <- NA
  df_test <- rbind(df_test, data_bss_pred)
}



#on fait les prévisions avec le paramètre p 
 df_test_p <- df_test
 for( i in (k+1):(93+k+1)) {
   # On construit une ligne de prédiction à faire par piezo (une ligne toutes les 98 pour le jeu de données)
   seq <- seq(i, nrow(df_test_p), by=(93+k+1))
   data_to_pred_tp <- data.frame(a=rep(NA,length(df_test_p[seq,2])))
   data_to_pred_p <- data.frame(a=rep(NA,length(df_test_p[seq,2])))
   for ( w in 1:k){
     data_to_pred_p <- cbind(data_to_pred_p, df_test_p[seq-w,1])
   }
   data_to_pred_p <- data_to_pred_p[,-1]
   colnames(data_to_pred_p) <- paste('p_t_',1:k,sep='')
   df_test_p[seq,1] = predict(modele_p,data_to_pred_p)
 }

#on fait les prediction avec les variables p et tp 
df_test_p_tp <- df_test
for( i in (k+1):(93+k+1)) {
  # On construit une ligne de prédiction à faire par piezo (une ligne toutes les 98 pour le jeu de données)
  seq <- seq(i, nrow(df_test_p_tp), by=(93+k+1))
  data_to_pred_tp <- data.frame(a=rep(NA,length(df_test_p_tp[seq,2])))
  data_to_pred_p <- data.frame(a=rep(NA,length(df_test_p_tp[seq,2])))
  for (v in 0:k){
    data_to_pred_tp <- cbind(data_to_pred_tp, df_test[seq-v,2])
  }
  data_to_pred_tp <- data_to_pred_tp[,-1]
  colnames(data_to_pred_tp) <- paste('tp_t_',0:k,sep='')
  for ( w in 1:k){
    data_to_pred_p <- cbind(data_to_pred_p, df_test_p_tp[seq-w,1])
  }
  data_to_pred_p <- data_to_pred_p[,-1]
  colnames(data_to_pred_p) <- paste('p_t_',1:k,sep='')
  data_to_pred <- data.frame(data_to_pred_tp,data_to_pred_p)
  df_test_p_tp[seq,1] = predict(modele_p_tp,data_to_pred)
}



#on fait les prédiction avec les variables p tp et les caractéristiques des piézomètre

df_test_p_tp_bdlisa <- tibble()
for ( bssc in code_bss ) {
  data_bss_pred <-  data %>% filter(bss == bssc) %>% select(p,tp,bss)
  data_bdlisa_bss <- bdlisa %>% filter(bss==bssc)%>% select(EtatEH,NatureEH,MilieuEH,OrigineEH,ThemeEH,bss)
  if (nrow(data_bss_pred) - h -k <1) {
    print(bss)
    next
  }
  data_bss_pred <-  data_bss_pred[(nrow(data_bss_pred) - h -k):nrow(data_bss_pred),] 
  data_bss_pred$gt <-  data_bss_pred[,1]
  data_bss_pred[(nrow(data_bss_pred) - h):nrow(data_bss_pred),1] <- NA
  data_bss_pred_bdlisa <- merge(data_bss_pred,data_bdlisa_bss,key='bss')
  df_test_p_tp_bdlisa <- rbind(df_test_p_tp_bdlisa, data_bss_pred_bdlisa)
  
}

for( i in (k+1):(93+k+1)) {
  # On construit une ligne de prédiction à faire par piezo (une ligne toutes les 98 pour le jeu de données)
  seq <- seq(i, nrow(df_test_p_tp_bdlisa), by=(93+k+1))
  data_to_pred_tp <- rep(NA)
  data_to_pred_p <- rep(NA)
  for (v in 0:k){
    data_to_pred_tp <- cbind(data_to_pred_tp, df_test_p_tp_bdlisa[seq-v,3])
  }
  data_to_pred_tp <- data_to_pred_tp[,-1]
  colnames(data_to_pred_tp) <- paste('tp_t_',0:k,sep='')
  for ( w in 1:k){
    data_to_pred_p <- cbind(data_to_pred_p, df_test_p_tp_bdlisa[seq-w,2]) 
  }
  data_to_pred_p <- data_to_pred_p[,-1]
  colnames(data_to_pred_p) <- paste('p_t_',1:k,sep='')
  data_to_pred_EH <- data.frame(EtatEH=df_test_p_tp_bdlisa[seq,5],NatureEH=df_test_p_tp_bdlisa[seq,6],MilieuEH=df_test_p_tp_bdlisa[seq,7],ThemeEH=df_test_p_tp_bdlisa[seq,9],OrigineEH=df_test_p_tp_bdlisa[seq,8]) 
  data_to_pred <- data.frame(data_to_pred_tp,data_to_pred_p,data_to_pred_EH)
  df_test_p_tp_bdlisa[seq,2] <-  predict(modele_p_tp_bdlisa,data_to_pred)
}
```

```{r}
rmse_p= sqrt(mean((as.vector(df_test_p$p)-as.vector(df_test_p$gt))^2))
rmse_p_tp= sqrt(mean((as.vector(df_test_p_tp$p)-as.vector(df_test_p_tp$gt))^2))
rmse_p_tp_bdlisa= sqrt(mean((as.vector(df_test_p_tp_bdlisa$p)-as.vector(df_test_p_tp_bdlisa$gt))^2))

# RMSE pour les 2000 piezos
## avec un historique de 10 -> les deux RMSE au alentours de 80 
## avec un historique de 100 -> les deux RMSE au alentours de 60
```


```{r}

serie1_pred= df_test_p_tp%>% filter(bss=="00327X0059/P11")
serie1 <- data %>% filter(bss=="00327X0059/P11") 
s1=rbind(data.frame(niveau=serie1$p,val='valeurs observés',t=1:366),data.frame(niveau=serie1_pred$p,val='valeurs prédites',t=266:366))
p <- ggplot(data=s1, aes(x=t, y=niveau, color=val))+
  geom_line()+
  xlab('time (day)')+
  ggtitle("00327X0059/P11")+
  theme_bw()
p
```





