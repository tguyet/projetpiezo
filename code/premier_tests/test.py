#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Guyet, Institut Agro/IRISA
"""
import json
import pandas as pd

import requests

url = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/stations'
params = {'code_commune': 71270, 'format': 'json', 'size': 20}

response = requests.get(url, params=params)
data=response.json()

for i in range(data['count']):
    print( data['data'][i] )
    print("------------")

    url = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/chroniques'
    params = {'code_bss':data['data'][i]['code_bss'], 'size':5000}
    response = requests.get(url, params=params)
    val=response.json()

    dates = [ val['data'][j]["date_mesure"] for j in range( val['count'] )]
    vals = [ val['data'][i]["profondeur_nappe"] for j in range( val['count'] )]

    df=pd.DataFrame({'t':pd.to_datetime(dates), 'p':vals})
    df.set_index('t',inplace=True)
    print(df)
