# -*- coding: utf-8 -*-
"""
Created on Tue May  4 16:42:39 2021
Script de chargement des données piezométriques
@author: Lola Beuchée
"""


parquet_installed=False
try:
    import pyarrow.parquet as pq
    parquet_installed=True 
except:
    pass

import json
import pandas as pd
import requests


def code_dep():
    code_departement=['01','02','03','04','05','06','07','08','09']
    for i in range(10,20):
        code_departement.append(str(i))
    code_departement+=['2A','2B']
    for i in range (21,69):
        code_departement.append(str(i))
    code_departement+=['69D','69M']
    for i in range (70,95):
        code_departement.append(str(i))
    code_departement+=['971','972','973','974','975','976','977','978','984','986','987','988','989']
    return code_departement
def code_depa():
    code_departement=['69D','69M']
    for i in range (70,95):
        code_departement.append(str(i))
    code_departement+=['971','972','973','974','975','976','977','978','984','986','987','988','989']
    return code_departement

code_departement=code_dep()
#code_departement=['35']
df_final=None
for dpt in code_departement:
    print(f'Téléchargement des données pour le dpt {dpt}')
    url = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/stations'
    params = {'code_departement': dpt, 'format': 'json', 'nb_mesures_piezo_min':1000}
    response = requests.get(url, params=params)
    data=response.json()
    print('\tNbre station:%d'%(data['count']))
    for i in range(data['count']):
        url = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/chroniques'
        params = {'code_bss':data['data'][i]['code_bss'], 'date_debut_mesure':'2015-01-01', 'size':5000}
        response = requests.get(url, params=params)
        val=response.json()
        if len(val['data']) == 0:
            print("\t %s -> pas de données"%(data['data'][i]['code_bss']))
            continue
        print("\t %s -> ajout %d mesures"%(data['data'][i]['code_bss'], len(val['data'])))
        dates = [ val['data'][j]["date_mesure"] for j in range(len(val['data']) )]
        vals = [ val['data'][i]["profondeur_nappe"] for i in range(len(val['data']) )]
        bss=[ data['data'][i]['code_bss']]*len(vals)
        df=pd.DataFrame({'t':pd.to_datetime(dates), 'p':vals, 'bss':bss})
        #df.set_index('t',inplace=True)
        if df_final is None:
             df_final=df
        else:
            df_final= pd.concat([df_final,df],ignore_index = True)
        print(df_final)
    if parquet_installed:
        df_final.to_parquet('fichier_01_%s.parquet'%dpt)
    filename = 'fichier_01_%s.csv'%dpt
    df_final.to_csv(filename, index=False)
    
    
    
    
    
    
    
