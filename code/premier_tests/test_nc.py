#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Pour les données Evapotranspiration (eto_month):
1) commencer par se connecter à http://www.indecis.eu/indices.php
2) choisir une carte d'indice climatique (ici )

Pour les données de pluies (rain_2020.nc) : voir le chargmeent dans load_cdsdata.py

@author: T. Guyet, Institut Agro/IRISA
"""

import pandas as pd
import numpy as np
import xarray as xr

ds=xr.open_dataset("eto_month.nc")

ds_rain = xr.open_dataset("rain_2020.nc")
