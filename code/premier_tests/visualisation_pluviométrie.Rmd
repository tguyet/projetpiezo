---
title: "stats descriptives pluviométrie"
output: html_document
---

```{r}
rm(list=ls()) 
rain_data = read_delim('../../data/rain_bss_2020.csv', ",")
pietzo_data= readRDS('../../data/prev_oct2020_sans_DOM-TOM.csv')
colnames(rain_data)=c("t",'tp','bss')
pietzo_data=pietzo_data[,-3]
colnames(pietzo_data)=c('bss','t','p')

data=merge(rain_data,pietzo_data,key=c('bss','t'))
```
Statistiques sur quelques séries pluviométriques
```{r}

code_bss=data$bss[-which(duplicated(data$bss)) ]
serie1 = data %>% filter(bss==code_bss[1]) %>% select(t,tp)
serie2=data%>% filter(bss==code_bss[2])%>%select(t,tp)
serie3=data%>% filter(bss==code_bss[3])%>% select(t,tp)
serie4=data%>% filter(bss==code_bss[4])%>%select(t,tp)
```

```{r}
# serie1
code_bss[1]
code_bss[2]
code_bss[3]
code_bss[4]

p <- ggplot(serie1, aes(x=t, y=tp)) + 
  geom_line() + 
  geom_smooth()+
  theme_bw()+ 
  xlab("Temps") + ylab("Pluviométrie")+
  ggtitle("00327X0059/P11")
p
#serie2
p <- ggplot(serie2, aes(x=t, y=tp)) + 
  geom_line() +
  geom_smooth()+
  theme_bw()+ 
  xlab("Temps") + ylab("Pluviométrie")+
  ggtitle("00406X0029/PAEP")
p
#serie3
p <- ggplot(serie3, aes(x=t, y=tp)) + 
  geom_line() + 
  geom_smooth()+
  theme_bw()+ 
  xlab("Temps") + ylab("Pluviométrie")+
  ggtitle("00493X0346/PZ1")
p
#serie4
p <- ggplot(serie4, aes(x=t, y=tp)) + 
  geom_line() + 
  geom_smooth()+
  theme_bw()+ 
  xlab("Temps") + ylab("Pluviométrie")+
  ggtitle("00501X0017/S1")
p
```

```{r}
# serie1
p <- ggplot(serie1, aes(y=tp)) + 
  geom_boxplot() + 
  theme_bw()+
  ylab("pluviométrie")
p
#serie2
p <- ggplot(serie2, aes(y=tp)) + 
  geom_boxplot() +
  theme_bw()+
  ylab("pluviométrie")
p
#serie3
p <- ggplot(serie3, aes(y=tp)) + 
  geom_boxplot() + 
  theme_bw()+
  ylab("pluviométrie")
p
#serie4
p <- ggplot(serie4, aes(y=tp)) + 
  geom_boxplot() + 
  theme_bw()+  
  ylab("pluviométrie")
p
```
Visualisation de dela pluvoimétrie et du niveau d'eau pour quelques bss
```{r}


code_bss=data$bss[-which(duplicated(data$bss)) ]

## bss 1
data_bss=data%>%filter(bss==code_bss[1])
plot.new() 
par(mar=c(4,4,3,5)) 
plot(data_bss$p,type='l',col="dark green",axes=F,xlab="",ylab="") 
axis(2,col="dark green",col.axis="dark green",at=seq(0, 100, by=2)) 
mtext("niveau des nappes phréatiques",side=2,line=2.5,col="dark green")  
par(new = T)
plot(data_bss$tp,type='l',col="dark red",axes=F,xlab="",ylab="") 
axis( 4 ,col="dark red",col.axis="dark red",at=seq(0, 0.03, by=0.005)) 
mtext("pluviométrie",side=4,line=2.5,col="dark red")
axis( 1 , ,col="black",col.axis="black",at=seq(0, 400, by=50)) 
mtext("temps en jours",side=1,line=2.5,col="black") 


## bss 2
data_bss=data%>%filter(bss==code_bss[2])
plot.new() 
par(mar=c(4,4,3,5)) 
plot(data_bss$p,type='l',col="dark green",axes=F,xlab="",ylab="") 
axis(2,col="dark green",col.axis="dark green",at=seq(0, 100, by=2)) 
mtext("niveau des nappes phréatiques",side=2,line=2.5,col="dark green")  
par(new = T)
plot(data_bss$tp,type='l',col="dark red",axes=F,xlab="",ylab="") 
axis( 4 ,col="dark red",col.axis="dark red",at=seq(0, 0.03, by=0.005)) 
mtext("pluviométrie",side=4,line=2.5,col="dark red")
axis( 1 , ,col="black",col.axis="black",at=seq(0, 400, by=50)) 
mtext("temps en jours",side=1,line=2.5,col="black") 

## bss 3
data_bss=data%>%filter(bss==code_bss[3])
plot.new() 
par(mar=c(4,4,3,5)) 
plot(data_bss$p,type='l',col="dark green",axes=F,xlab="",ylab="") 
axis(2,col="dark green",col.axis="dark green",at=seq(0, 100, by=2)) 
mtext("niveau des nappes phréatiques",side=2,line=2.5,col="dark green")  
par(new = T)
plot(data_bss$tp,type='l',col="dark red",axes=F,xlab="",ylab="") 
axis( 4 ,col="dark red",col.axis="dark red",at=seq(0, 0.03, by=0.005)) 
mtext("pluviométrie",side=4,line=2.5,col="dark red")
axis( 1 , ,col="black",col.axis="black",at=seq(0, 400, by=50)) 
mtext("temps en jours",side=1,line=2.5,col="black") 

## bss 4
data_bss=data%>%filter(bss==code_bss[4])
plot.new() 
par(mar=c(4,4,3,5)) 
plot(data_bss$p,type='l',col="dark green",axes=F,xlab="",ylab="") 
axis(2,col="dark green",col.axis="dark green",at=seq(0, 100, by=2)) 
mtext("niveau des nappes phréatiques",side=2,line=2.5,col="dark green")  
par(new = T)
plot(data_bss$tp,type='l',col="dark red",axes=F,xlab="",ylab="") 
axis( 4 ,col="dark red",col.axis="dark red",at=seq(0, 0.03, by=0.005)) 
mtext("pluviométrie",side=4,line=2.5,col="dark red")
axis( 1 , ,col="black",col.axis="black",at=seq(0, 400, by=50)) 
mtext("temps en jours",side=1,line=2.5,col="black") 

```

```{r}
p <- ggplot(data, aes(y=tp)) + 
  geom_boxplot() + 
  theme_bw()+
  ylab("pluviométrie")
p

```

```{r}
#on sépare les données en données mensuelles 
df_month=data.frame()
for ( i in 1:length(code_bss)) {
  serie1 = data %>% filter(bss==code_bss[i]) %>% select(t,tp)
  serie1$year = format(serie1$t, format = "%Y")
  serie1$month =as.numeric(format(serie1$t, format = "%m"))
  serie1$yearmonth=ymd(paste0(serie1$year,'-',serie1$month,'-01'))
  
  ss <- serie1 %>% group_by(year, yearmonth) %>% dplyr::summarise(tp = mean(tp, na.rm = TRUE), .groups='drop') %>% ungroup() %>% select("yearmonth","tp")
  df_month=rbind(df_month,data_frame(bss=rep(code_bss[i],each=12),yearmonth=ss$yearmonth,tp=ss$tp))
}
df_month$month =as.factor(format(df_month$yearmonth, format = "%m"))
```

```{r}
p <- ggplot(df_month, aes(x=month, y=tp))+
  geom_boxplot()+
  geom_jitter(color = "red", alpha = 0.2)+
  theme_bw()+
  xlab("Mois") + ylab("Pluviométrie")
p

```
```{r}
p = ggplot(data, aes(x=t, y=tp, group=bss))+
  geom_line()+
  theme_bw()+
  xlab("Time") + ylab("Pluviométrie")
p
```

