#
#
# author: L. Beuchée, T. Guyet
# date: 7/2021
#

# Chargement des librairies
if (Sys.info()["nodename"]=="mandarinier.irisa.fr") {
    .libPaths('/home/Rlibs')
	library(tidyverse, lib.loc="/home/Rlibs")
	library(tsibble, lib.loc="/home/Rlibs")
	library(e1071, lib.loc="/home/Rlibs")
	library(lubridate, lib.loc="/home/Rlibs")
	library(forecast, lib.loc="/home/Rlibs")
	library(stringr, lib.loc="/home/Rlibs")
} else {
	library(tidyverse)
	library(tsibble)
	library(e1071)
	library(lubridate)
	library(lmtest)
	library(forecast)
	library(stringr)
}

#Configuration des variables d'environnement utiles
rm(list=ls()) 
rep_data="../../data2/"
source("utils.R")

#chargement des données
data <-  read_delim(paste0(rep_data,'dataset_2015_2021.csv'), ",")
code_bss <-data$bss %>% unique()

data <- distinct(data, bss, time, .keep_all= TRUE)


# il y a plusieurs données manquantes pour les données de niveau d'eau, on fait donc une imputation de données manquantes

#D'abord, on se débarrasse des séries pour lesquelles il manque trop de données
#code_bss=(data %>% filter( is.na(p)) %>% group_by( bss) %>% summarize( n=n()) %>% filter(n<100))$bss
# "00487X0015/S1""01516X0004/S1""02267X0030/S1" "02648X0020/S1"
code_bss = (data %>% group_by(bss) %>% summarize( c = sum(is.na(p))) %>% filter( c==0))$bss
data = data %>% filter( bss %in% code_bss )

# On applique une imputation sur chaque sous-groupe
ret = data %>% group_by( bss ) %>% group_map(
  ~ as.vector(na.interp(.x$p))
)
data$p = unlist(ret)


h=93 # horizon de prédiction
k=100 # nombre de valeurs à utiliser dans le passer




all_rmsse <- NULL
for ( j in 1:length(code_bss)){
#for ( j in 1:1){
  cat(paste0("processing time series: ",j,"/",length(code_bss),"\n"))
  data_bss <-  data %>% filter(bss ==code_bss[j]) %>% select(p,tp,e,bss)
  train <-  data_bss[2:(nrow(data_bss) - h),]
  train$p_1 <-data_bss[1:(nrow(data_bss) - (h+1)),]$p
  TN=sum( (train$p-train$p_1)^2 )/(nrow(data_bss)-1)
  
  test <- data_bss[(nrow(data_bss)-h+1):(nrow(data_bss)),]
  #on creer la matrice qui nous permettras d'effectuer les modèles lm et svr
  df <- fct_matrice(train,k)
  
  cat(paste0("\tlearn models\n"))
  #on effectue le modèles qui nous permettrons d'effectuer les prédictions pour lm et svr
  lm_p_tp_e <- myreg(df,1:((k*3)+2),(k*3+3))
  lm_p_tp <- myreg(df,(k+2):(k*3+2),(k*3+3))
  lm_p <- myreg(df,(k*2+3):(k*3+2),(k*3+3))
  svr_p_tp_e <- modele_svm(df,1:((k*3)+2),(k*3+3))
  svr_p_tp <- modele_svm(df,(k+2):(k*3+2),(k*3+3))
  svr_p <- modele_svm(df,(k*2+3):(k*3+2),(k*3+3))
  rf_p_tp_e <- modele_rf(df,1:((k*3)+2),(k*3+3))
  rf_p_tp <- modele_rf(df,(k+2):(k*3+2),(k*3+3))
  rf_p <- modele_rf(df,(k*2+3):(k*3+2),(k*3+3))
  
  
  df_test <- tibble()
  #préparation du jeu de donnée pour les prédiction de lm et svr
  data_bss_pred = data %>% filter(bss == code_bss[j]) %>% select(p,tp,e,bss)
  if (nrow(data_bss_pred) - h - k <1) {
    print(bss)
    next
  }
  data_bss_pred = data_bss_pred[(nrow(data_bss_pred) - h -k):nrow(data_bss_pred),] 
  data_bss_pred$gt = data_bss_pred$p
  data_bss_pred[(nrow(data_bss_pred) - h):nrow(data_bss_pred),1] <- NA
  df_test <- rbind(df_test, data_bss_pred)
  
  for( classifier in c('lm','svr','rf') ) {
    for( features in c('p','p_tp','p_tp_e') ) {
      model=paste0(classifier,"_",features)
      cat(paste0("\ttest model: ",model,"\n"))
      df_test_p <- df_test
      for (i in (k+1):(93+k+1)){
        data_to_pred_tp <- rep(NA)
        data_to_pred_e <- rep(NA)
        data_to_pred_p <- rep(NA)
        for (v in 0:k){
          data_to_pred_tp <- cbind(data_to_pred_tp, df_test_p[i-v,2])
        }
        data_to_pred_tp <- data_to_pred_tp[,-1]
        colnames(data_to_pred_tp) <- paste('tp_',0:k,sep='')
        for (x in 0:k){
          data_to_pred_e <- cbind(data_to_pred_e, df_test_p[i-v,3])
        }
        data_to_pred_e <- data_to_pred_e[,-1]
        colnames(data_to_pred_e) <- paste('e_',0:k,sep='')
        
        for ( w in 1:k){
          data_to_pred_p <- cbind(data_to_pred_p, df_test_p[i-w,1]) 
        }
        data_to_pred_p <- data_to_pred_p[,-1]
        colnames(data_to_pred_p) <- paste('p_',1:k,sep='')
        data_to_pred <- cbind(data_to_pred_e,data_to_pred_tp,data_to_pred_p)
        
        df_test_p[i,1] <-  predict(eval(parse(text=model)), data_to_pred)
      }
      
      ldf <- data.frame(id_piezo=code_bss[j],
                        id_method_ML=classifier,
                        type="Local", r=k,
                        use_exo_rain=str_detect(features,"tp"),
                        use_exo_eto=str_detect(features,"e"),
                        use_exo_bdlisa=FALSE, approx_exo=FALSE,
                        rmsse=sqrt((mean((as.vector(df_test_p$p)-as.vector(df_test_p$gt))^2))/TN),
                        mse=mean((as.vector(df_test_p$p)-as.vector(df_test_p$gt))^2) )
      all_rmsse <- rbind(all_rmsse,ldf)
    }
  }
  
  #ARIMA
  
  # prediction arima avec p 
  fit_p <- auto.arima(train$p)
  fcast <- forecast(fit_p, h=h)
  ldf <- data.frame(id_piezo=code_bss[j],
                    id_method_ML="ARIMA",
                    type="Local", r=k,
                    use_exo_rain=FALSE,
                    use_exo_eto=FALSE,
                    use_exo_bdlisa=FALSE, approx_exo=FALSE,
                    rmsse=sqrt(mean((as.vector(test$p)-as.vector(fcast[["mean"]]))^2)/TN),
                    mse=mean((as.vector(test$p)-as.vector(fcast[["mean"]]))^2)
  )
  all_rmsse <- rbind(all_rmsse,ldf)
  
  #prediction arima avec p et tp 
  fit_p_tp <- auto.arima(train$p, xreg = train$tp)
  fcast <- forecast(fit_p_tp, xreg = test$tp, h = h)
  ldf <- data.frame(id_piezo=code_bss[j],
                    id_method_ML="ARIMA",
                    type="Local", r=NA,
                    use_exo_rain=TRUE,
                    use_exo_eto=FALSE,
                    use_exo_bdlisa=FALSE, approx_exo=FALSE,
                    rmsse=sqrt(mean((as.vector(test$p)-as.vector(fcast[["mean"]]))^2)/TN),
                    mse=mean((as.vector(test$p)-as.vector(fcast[["mean"]]))^2)
  )
  all_rmsse <- rbind(all_rmsse,ldf)
  fit_p_tp_e <- auto.arima(train$p, xreg = cbind(train$tp,train$e))
  fcast <- forecast(fit_p_tp_e, xreg = cbind(test$tp,test$e), h = h)
  ldf <- data.frame(id_piezo=code_bss[j],
                    id_method_ML="ARIMA",
                    type="Local", r=NA,
                    use_exo_rain=TRUE,
                    use_exo_eto=TRUE,
                    use_exo_bdlisa=FALSE, approx_exo=FALSE,
                    rmsse=sqrt(mean((as.vector(test$p)-as.vector(fcast[["mean"]]))^2)/TN),
                    mse=mean((as.vector(test$p)-as.vector(fcast[["mean"]]))^2)
  )
  all_rmsse <- rbind(all_rmsse,ldf)
  #prédiction arima avec p, tp et e 
  
  
  
  #   # prédiction avec prophet
  #   data_bss <-  data %>% filter(bss ==code_bss[j]) %>% select(t,p)
  #   test <- data_bss[(nrow(data_bss) - h+1):(nrow(data_bss)),]
  #   train_proph <-  data_bss[2:(nrow(data_bss) - h),]
  #   names(train_proph) <- c('ds', 'y') 
  #   m <- prophet(train_proph)
  #   future <- make_future_dataframe(m, periods=h)
  #   proph <- predict(m, future)
  #   prev_proph <- proph[(nrow(proph) - h+1):(nrow(proph)),]
  #   
  #   rmsse_prophet[j] <- sqrt((mean((as.vector(test$p)-as.vector(prev_proph$trend))^2))/TN)
  
  write.table(all_rmsse,
            paste0(rep_data,"local_sansapprox.tmp.csv"),
            row.names=FALSE,
            sep="\t",
            dec="." )
}

write.table(all_rmsse,
            paste0(rep_data,"local_sansapprox.csv"),
            row.names=FALSE,
            sep="\t",
            dec="." )


