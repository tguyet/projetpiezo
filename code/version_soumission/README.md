## Procédure pour la génération de résultats pour la compétition EGC

Cette procédure permet de faire des prédictions pour les 18 piézomètres visés par la compétition EGC


0) Commencer par éditer les scripts en indiquant la date à partir de laquelle la prédiction doit être faite

* dans le script `load_dataset.py` définir la date dans la variable `date_fin` (l.19)
* dans le script `make_predictions.R` définir la date dans la variable `prediction_date` (l.32)

Attention: il faut vérifier que le chargement des données a permis de collecter les données jusqu'au jour précédent la date souhaitée. 
Il risque d'y avoir des soucis dans le cas contraire.

1) Lancer la récupération des données

```
python3 load_dataset.py
```

Cette étape engendre un fichier de données `final_dataset.csv`.


2) Lancer dans un second temps la procédure de prédiction

```
Rscript make_prediction.R
```

Cette étape engendre le fichier de réponse attendu pour la compétition `Beuchee_Guyet_Malinowski.csv` près à soumettre (noms de colonne, format et nom de fichier ajustés à la demande).




