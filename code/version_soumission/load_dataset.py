#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generation des jeux de données

 
@author: T. Guyet, Institut Agro/IRISA
"""

import pandas as pd
import numpy as np
import xarray as xr
from tqdm import tqdm
import requests

annee_debut=2015

date_debut=str(annee_debut)+'-01-01'
date_fin='2021-10-15'

list_bss=["00471X0095/PZ2013","00487X0015/S1","00755X0006/S1"  ,"00762X0004/S1","00766X0004/S1","01258X0020/S1","01516X0004/S1","01584X0023/LV3","02206X0022/S1","02267X0030/S1","02603X0009/S1","02648X0020/S1","02706X0074/S77-20","03124X0088/F","04398X0002/SONDAG","06505X0080/FORC","07223C0113/S","07476X0029/S"]


#######################################################"
# On commence par le niveau des eaux pour les piezos
print("Number of BSS time series to load: ", len(list_bss))
levels=[]
for bss in tqdm(list_bss):
    url = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/chroniques'
    params = {'code_bss':bss, 'date_debut_mesure':date_debut, 'date_fin_mesure':date_fin, 'size':5000}
    response = requests.get(url, params=params)
    val=response.json()
    dates = [ val['data'][j]["date_mesure"] for j in range(len(val['data']) )]
    vals = [ val['data'][i]["profondeur_nappe"] for i in range(len(val['data']) )]
    bss=[ bss ]*len(vals)
    levels.append( pd.DataFrame({'t':pd.to_datetime(dates), 'p':vals, 'bss':bss}) )

levels = pd.concat(levels)
levels=levels.rename(columns={"t":"time"}).set_index(['bss','time'])

#dataset=rain.join(alleto).join(levels)
levels.to_csv("final_dataset.csv")


   
