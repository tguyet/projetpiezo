# Organisation tâche Lola

## Grandes étapes prévues pour le stage
0. chargement des données, visu des données, données manquantes, exploration, corrélation entre variables, autocorrélation, regarder les différences annuelles (visu)
 * objectifs 
  - se familiariser avec les données
  - savoir quelles sont les données disponibles
  - savoir quels sont les 'individus statistiques' qu'on peut créer pour faire de l'analyse
  - créer un "beau" jeu de données : "beau" = bien formaté, facile d'utilisation pour différentes tâches d'analyse, bien documenté sur comment il a été obtenu
  - avoir des idées d'exploration
1. faire du clustering des séries temporelles : kmeans / DTW
2. faire des modèles types ARIMA / pour les 20000 + *clustering des paramètres du modèle*
 * ARIMAX ou autre
3. autres types de modélisations
 * modélisation bayésienne 
  - identification de variables d'influence
  - mise en place d'une structure de modèle type réseau bayésien
  - apprentissage et évaluation du modèle (librairie de modélisation générique MCMC, avec Python: https://emcee.readthedocs.io/en/stable/
 * Regarder le modèle LDA ??
4. redaction d'un rapport

# Notes de réunion

## Lundi 3 mai

* Réunion d'arrivée du lundi 
* Utilisation de l'adresse mail IRISA ?
* Faire un cahier de laboratoire (document en ligne ? ou sur le Git): il faut que cela soit simple pour toi et partagé !
* Utilisation du Git
 - Télécharger un outil de gestion https://git-scm.com/download/win
 - Prendre les bonnes habitudes dès le début
  1. faire un 'pull' en début de travail
  2. faire des petits commits
  3. faire un 'push' en fin de travail
 - Des problèmes de merge ?? pas de panique !
 - quelques règles : 
  - ne pas mettre les fichiers intermédiaires
  - GIT n'est pas fait pour stocker des données ! Préférer l'utilisation
* Récupération du code de chargement des données
* Python / R : je ne sais plus ! (premiers codes en Python)
* Modalités de communication (Teams ??) : à voir mardi avec Simon
* point en présentiel mardi (arrangé avec Gaëlle)


